package com.jiovio.olivewear.rural.savemom.fragment.measure.weight

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jiovio.olivehealth.rural.savemom.R
import com.jiovio.olivewear.rural.savemom.fragment.BaseFragment
import com.jiovio.olivewear.rural.savemom.fragment.measure.MeasureDoneFragment
import com.jiovio.olivewear.rural.savemom.ui.MeasureActivity
import kotlinx.android.synthetic.main.activity_measure.*
import kotlinx.android.synthetic.main.fragment_measure_height.*
import org.jetbrains.anko.support.v4.toast

/**
 * Created by abara on 29/10/17.
 */
class MeasureHeightFragment : BaseFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_measure_height, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        setTitle(R.string.measure_title_weight_test)

        val finishButton = (activity)!!.measure_btn
        finishButton.text = "Finish"
        finishButton.setOnClickListener {

            val heightString = measure_height_box.text.toString()
            if (heightString.isNotEmpty()) {
                val height = heightString.toDouble()
                val weight = 65

                val bmi = (weight / Math.pow((height / 100), 2.0)).toInt()

                val lastWeight = (activity as MeasureActivity).getLastWeight()
                val gainedWeight = if (lastWeight != null) weight - lastWeight else 0

                nextFragment(MeasureDoneFragment.getInstance(MeasureActivity.FragmentType.WEIGHT.name, weightOrSpoPair = Pair(Pair(weight, bmi), Pair(weight, gainedWeight))), false)
            } else {
                toast("Enter height value")
            }

        }

    }

}