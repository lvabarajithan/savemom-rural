package com.jiovio.olivewear.rural.savemom.mcloud

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.IBinder
import android.os.Message
import android.os.Messenger
import android.os.RemoteException
import android.util.Log
import com.medzone.mcloud.background.BluetoothMessage
import com.medzone.mcloud.background.MMeasureService
import com.medzone.mcloud.background.util.BluetoothUtils
import java.util.*

object MeasureHelper {

    val BLOOD_PRESSURE = 1
    val BLOOD_PRESSURE_ARM = 12
    val BLOOD_PRESSURE_ALL = 13
    val BLOOD_OXYGEN = 2
    val BLOOD_OXYGEN_ALL = 22
    val BLOOD_OXYGEN_TWO = 21
    val OXYGEN_RING = 200
    val EAR_TEMPERATURE = 3
    val BLOOD_SUGAR = 4
    val FETAL_HEART = 5
    val ECG = 6
    val ECG2 = 206
    val URINE_ANALYSIS = 7
    val BASE_TEMPERATURE = 8
    val BODY_FAT = 9
    val ID_CARD = 100
    val WELFARE_CARD = 101
    val WELFARE_CARD2 = 104
    val SJM = 201

    val SHOW_DEVICE_LIST = 0x100
    val UPDATE_DEVICE_LIST = 0x101
    val HIDE_DEVICE_LIST = 0x102
    val UPDATE_STATUS = 0x200
    val DEVICE_DETECTED = 0x201
    val MEASURE_RESULT = 0x202

    interface ICommandCallback {
        fun handleMessage(msg: Message)
    }

    val mDeviceLists = ArrayList<String>()
    lateinit var mDevAddress: String
    val mRssiLists = ArrayList<Int>()
    var mRssi: Int? = null
    var mType: Int = 0
    private var mServerMessenger: Messenger? = null
    private var mClientMessenger: Messenger? = null
    val listeners = ArrayList<ICommandCallback>()

    private var serviceConnection: ServiceConnection = object : ServiceConnection {

        override fun onServiceConnected(name: ComponentName, binder: IBinder) {
            mServerMessenger = Messenger(binder)
            Log.d("MeasureHelper","+onServiceConnected()")
        }

        override fun onServiceDisconnected(name: ComponentName) {
            Log.d("MeasureHelper","+onServiceDisconnected()")
        }
    }

    fun bind(context: Context) {
        if (mServerMessenger != null) {
            return
        }
        mClientMessenger = Messenger(MessageHandler)
        val intent = Intent(context, MMeasureService::class.java)
        context.bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE)
    }

    fun unbind(context: Context) {
        Log.d("MeasureHelper","+unbind()")
        context.unbindService(serviceConnection)
        mServerMessenger = null
    }

    fun addListener(listener: ICommandCallback) {
        if (!listeners.contains(listener)) {
            Log.d("MeasureHelper","addListener():" + listener.hashCode())
            listeners.add(listener)
        }
    }

    fun removeListener(listener: ICommandCallback) {
        if (listeners.contains(listener)) {
            listeners.remove(listener)
            Log.d("MeasureHelper","removeListener():" + listener.hashCode())
        }
    }

    fun open(type: Int, address: String) {
        mType = type
        mDeviceLists.clear()
        mRssiLists.clear()
        val deviceAddress = address
        if (deviceAddress !== "" && deviceAddress !== ":")
            mDeviceLists.add(deviceAddress)
        open(deviceAddress)
    }

    private fun open(deviceAddress: String) {
        mDevAddress = deviceAddress
        sendCommand(BluetoothMessage.msg_open, 0, mDevAddress)
    }

    fun query() {
        sendCommand(BluetoothMessage.msg_send, BluetoothUtils.QUERY_TERMAINAL)
    }

    fun measure() {
        sendCommand(BluetoothMessage.msg_send, BluetoothUtils.START_MEASURE)
    }

    private fun record() {
        sendCommand(BluetoothMessage.msg_send, BluetoothUtils.RECORD)
    }

    private fun progress() {
        sendCommand(BluetoothMessage.msg_send, BluetoothUtils.MEASURE_PROGRESS)
    }

    private fun cancelMeasure() {
        sendCommand(BluetoothMessage.msg_send, BluetoothUtils.PAUSE_MEASURE)
    }

    private fun calibrateTime() {
        sendCommand(BluetoothMessage.msg_send, BluetoothUtils.CALABRATE_TIME)
    }

    private fun calibrate() {
        sendCommand(BluetoothMessage.msg_send, BluetoothUtils.CALABRATE)
    }

    private fun queryLatest() {
        sendCommand(BluetoothMessage.msg_send, BluetoothUtils.RESEND_LAST_DATA)
    }

    private fun testBluetooth() {
        sendCommand(BluetoothMessage.msg_send, BluetoothUtils.TEST_BT)
    }

    private fun clearData() {
        sendCommand(BluetoothMessage.msg_send, BluetoothUtils.CLEAR_DATA)
    }

    fun close() {
        sendCommand(BluetoothMessage.msg_close, 0)
    }

    private fun getStatus() {
        sendCommand(BluetoothMessage.msg_get_status, 0)
    }

    private fun queryData() {
        sendCommand(BluetoothMessage.msg_send, BluetoothUtils.QUERY_DATA.toInt())
    }

    private fun maternalAndChild() {
        sendCommand(BluetoothMessage.msg_send, BluetoothUtils.MATERNAL_AND_CHILD)
    }

    private fun closeBluetooth() {
        sendCommand(BluetoothMessage.msg_send, BluetoothUtils.CLOSE_BT)
    }

    private fun closeDevice() {
        sendCommand(BluetoothMessage.msg_send, BluetoothUtils.CLOSE_DEVICE)
    }

    private fun setTermainalTime() {
        sendCommand(BluetoothMessage.msg_send, BluetoothUtils.SET_TERMAINAL_TIME)
    }

    private fun cache() {
        sendCommand(BluetoothMessage.msg_send, BluetoothUtils.CACHE)
    }

    private fun setBlackTime() {
        sendCommand(BluetoothMessage.msg_send, BluetoothUtils.SET_BLACK_TIME)
    }

    private fun setUserInfo() {
        sendCommand(BluetoothMessage.msg_send, BluetoothUtils.SET_USER_INFO)
    }

    private fun getInstanceData() {
        sendCommand(BluetoothMessage.msg_send, BluetoothUtils.INSTANT_DATA)
    }

    private fun getHistoryData() {
        sendCommand(BluetoothMessage.msg_send, BluetoothUtils.HISTORY_DATA)
    }

    private fun detectStaticPressure() {
        sendCommand(BluetoothMessage.msg_send, BluetoothUtils.STATIC_PRESSURE)
    }

    private fun setting() {
        sendCommand(BluetoothMessage.msg_send, BluetoothUtils.SETTING)
    }

    private fun send(cmd: Int) {
        sendCommand(BluetoothMessage.msg_send, cmd)
    }

    private fun send(cmd: Int, params: ByteArray) {
        sendCommand(BluetoothMessage.msg_send, cmd, params)
    }

    private fun sendCommand(what: Int, cmd: Int, param: String) {
        if (mServerMessenger == null) return

        val msg = Message.obtain(null, 0)
        msg.what = what
        msg.arg1 = mType or (cmd shl 16)
        msg.arg2 = cmd
        msg.obj = param
        msg.replyTo = mClientMessenger
        try {
            mServerMessenger?.send(msg)
        } catch (e: RemoteException) {
            e.printStackTrace()
        }
    }

    private fun sendCommand(what: Int, cmd: Int, param: ByteArray) {
        if (mServerMessenger == null) return

        val result = HashMap<String, ByteArray>()
        result.put("data", param)
        val msg = Message.obtain(null, 0)
        msg.what = what
        msg.arg1 = mType
        msg.arg2 = cmd
        msg.obj = result
        msg.replyTo = mClientMessenger
        try {
            mServerMessenger?.send(msg)
        } catch (e: RemoteException) {
            e.printStackTrace()
        }

    }

    private fun sendCommand(what: Int, cmd: Short) {
        sendCommand(what, cmd.toInt())
    }

    private fun sendCommand(what: Int, cmd: Int) {
        if (mServerMessenger == null) return

        val msg = Message.obtain(null, 0)
        msg.what = what
        msg.arg1 = mType
        msg.arg2 = cmd
        msg.obj = null
        msg.replyTo = mClientMessenger
        try {
            mServerMessenger?.send(msg)
        } catch (e: RemoteException) {
            e.printStackTrace()
        }
    }
}
