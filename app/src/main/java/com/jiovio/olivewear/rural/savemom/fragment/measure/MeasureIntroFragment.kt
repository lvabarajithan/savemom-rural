package com.jiovio.olivewear.rural.savemom.fragment.measure

import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jiovio.olivehealth.rural.savemom.R
import com.jiovio.olivewear.rural.savemom.fragment.BaseFragment
import com.jiovio.olivewear.rural.savemom.ui.MeasureActivity
import kotlinx.android.synthetic.main.activity_measure.*
import kotlinx.android.synthetic.main.fragment_measure_intro.*
import org.jetbrains.anko.textColor

/**
 * Created by abara on 20/10/17.
 */
class MeasureIntroFragment : BaseFragment() {

    companion object {
        private val EXTRA_TYPE = "extra_type"
        private const val TYPE_UNKNOWN = "unknown"
        fun getInstance(fragmentType: String): MeasureIntroFragment {
            val fragment = MeasureIntroFragment()
            val args = Bundle()
            args.putString(EXTRA_TYPE, fragmentType)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_measure_intro, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        (activity)!!.measure_btn.visibility = View.GONE

        toolbarColor()
        val type = arguments!!.getString(EXTRA_TYPE, MeasureActivity.FragmentType.BP.name)

        val title = getTestTitle(type)
        setTitle(title)

        fragment_intro_title.text = resources.getString(title)
        fragment_intro_title.textColor = getColor(type)

        if (type == MeasureActivity.FragmentType.WEIGHT.name) {
            fragment_intro_skip.visibility = View.GONE
        }

        fragment_intro_go.setOnClickListener {
            nextFragment(MeasureConnFragment.getInstance(type))
        }

        fragment_intro_skip.setOnClickListener {
            val skipFragmentType = getSkipFragmentType(type)
            if (skipFragmentType != TYPE_UNKNOWN) {
                nextFragment(MeasureIntroFragment.getInstance(getSkipFragmentType(type)))
            }
        }

    }

    private fun getColor(type: String): Int = ContextCompat.getColor(activity!!, compareColorFor(type))

    private fun compareColorFor(type: String): Int = when (type) {
        MeasureActivity.FragmentType.BP.name -> R.color.colorTeal
        MeasureActivity.FragmentType.WEIGHT.name -> R.color.colorRedLite
        else -> R.color.colorBlackLight
    }

    private fun getSkipFragmentType(type: String): String = when (type) {
        MeasureActivity.FragmentType.BP.name -> MeasureActivity.FragmentType.SPO.name
        MeasureActivity.FragmentType.SPO.name -> MeasureActivity.FragmentType.WEIGHT.name
        else -> TYPE_UNKNOWN
    }

    override fun onDestroyView() {
        super.onDestroyView()
        (activity)!!.measure_btn.visibility = View.VISIBLE
    }

}