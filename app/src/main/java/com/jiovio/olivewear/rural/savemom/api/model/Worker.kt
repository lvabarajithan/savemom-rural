package com.jiovio.olivewear.rural.savemom.api.model

/**
 * Created by abara on 10/09/17.
 */
data class Worker(
        internal var success: Boolean = false,
        internal var data: WorkerData? = null,
        internal val token: String? = null
)

data class WorkerData(
        internal var _id: String? = null,
        internal var userid: String? = null,
        internal var healthworkername: String? = null,
        internal var email: String? = null,
        internal var mobile: String? = null,
        internal var qualification: String? = null,
        internal var designation: String? = null,
        internal var address: String? = null,
        internal var experience: String? = null,
        internal var yearofexperience: String? = null,
        internal var workingaddress: String? = null,
        internal var works: ArrayList<Work>? = null,
        internal var villages: ArrayList<Village>? = null
)

data class Work(
        internal var count: String? = null,
        internal var hours: String? = null,
        internal var date: String? = null,
        internal var time: String? = null,
        internal var villagename: String? = null
)

data class Village(
        internal var villagename: String? = null
)

data class WorkerBody(internal var userid: String = "")