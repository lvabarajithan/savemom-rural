package com.jiovio.olivewear.rural.savemom.util

import android.content.Context
import android.content.Context.INPUT_METHOD_SERVICE
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.os.Build
import android.os.IBinder
import android.support.v4.app.ActivityCompat
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentTransaction
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.AppCompatRadioButton
import android.text.format.DateFormat
import android.util.Base64
import android.view.inputmethod.InputMethodManager
import com.google.gson.Gson
import com.jiovio.olivehealth.rural.savemom.R
import com.jiovio.olivewear.rural.savemom.api.model.Worker
import org.jetbrains.anko.defaultSharedPreferences
import org.jetbrains.anko.toast
import java.io.ByteArrayOutputStream
import java.util.*

/**
 * Created by abara on 16/08/17.
 */

// Context
fun Context.parseColor(color: Int): Int = ContextCompat.getColor(this, color)

fun Context.hideKeyboard(windowToken: IBinder?) {
    try {
        val inputMethodManager = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(windowToken, 0)
    } catch (e: Exception) {
        // Nothing
    }
}

// Fragment
fun checkYesOrNo(yesOrNoString: Boolean?, yesBox: AppCompatRadioButton, noBox: AppCompatRadioButton) {

    if (yesOrNoString != null) {

        if (yesOrNoString) {
            yesBox.isChecked = true
        } else {
            noBox.isChecked = true
        }

    } else {
        noBox.isChecked = true
    }

}

fun checkYesOrNo(yesOrNoString: String?, yesBox: AppCompatRadioButton, noBox: AppCompatRadioButton) {

    if (yesOrNoString != null) {
        if ("yes".contentEquals(yesOrNoString.toLowerCase())) {
            yesBox.isChecked = true
        } else {
            noBox.isChecked = true
        }
    } else {
        noBox.isChecked = true
    }

}


// Activity
val PERMISSION_REQUEST_CAMERA = 99

fun AppCompatActivity.checkForPermission(permission: String, listener: (granted: Boolean) -> Unit) {

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

        if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
                checkForPermission(permission, listener)
                toast("Camera permission is required!")
            } else {
                ActivityCompat.requestPermissions(this, arrayOf(permission), PERMISSION_REQUEST_CAMERA)
            }

        } else {
            listener(true)
        }

    } else {
        listener(true)
    }

}

// Preferences
var Context.isLoggedIn: Boolean
    set(value) = defaultSharedPreferences.edit().putBoolean("logged_in", value).apply()
    get() = defaultSharedPreferences.getBoolean("logged_in", false)

var Context.workerPref: Worker
    set(value) = defaultSharedPreferences.edit().putString("worker_object", Gson().toJson(value)).apply()
    get() = Gson().fromJson(defaultSharedPreferences.getString("worker_object", "{}"), Worker::class.java)

var Context.accessTokenPref: String
    set(value) = defaultSharedPreferences.edit().putString("access_token", value).apply()
    get() = defaultSharedPreferences.getString("access_token", "")

// Fragment Transaction
fun FragmentManager.commitTransaction(code: FragmentTransaction.() -> FragmentTransaction) = beginTransaction().code().commit()

// RadioButton
fun AppCompatRadioButton.getCheckedString(defaultText: Int = R.string.radio_button_action_no): String = if (isChecked) {
    text.toString()
} else {
    resources.getString(defaultText)
}.toLowerCase()

// Tuples
infix fun <A, B, C> Pair<A, B>.to(that: C): Triple<A, B, C> = Triple(this.first, this.second, that)

// Calendar
infix fun Calendar.formatAs(inFormat: String): String = DateFormat.format(inFormat, this).toString()

infix fun Calendar.formatNowAs(inFormat: String): String {
    timeInMillis = System.currentTimeMillis()
    return formatAs(inFormat)
}

//Bitmap
fun Bitmap.asBase64String(): String {
    val baos = ByteArrayOutputStream()
    compress(Bitmap.CompressFormat.PNG, 30, baos)
    val bytes = baos.toByteArray()
    return Base64.encodeToString(bytes, Base64.DEFAULT)
}