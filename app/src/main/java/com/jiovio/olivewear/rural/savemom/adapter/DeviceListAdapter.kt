package com.jiovio.olivewear.rural.savemom.adapter

import android.bluetooth.BluetoothDevice
import android.support.v7.widget.AppCompatButton
import android.support.v7.widget.AppCompatTextView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jiovio.olivehealth.rural.savemom.R
import com.jiovio.olivewear.rural.savemom.data.OliveWear
import com.jiovio.olivewear.rural.savemom.util.ProfileUtils

/**
 * Created by abara on 02/09/17.
 */
class DeviceListAdapter(val deviceList: ArrayList<OliveWear> = ArrayList<OliveWear>(),
                        val onDeviceSelect: OnDeviceSelect) :
        RecyclerView.Adapter<DeviceListAdapter.DeviceItemViewHolder>() {

    fun addDevice(device: OliveWear) {
        if (deviceList.contains(device)) {
            val index = deviceList.indexOf(device)
            deviceList[index] = device
            notifyItemChanged(index)
        } else {
            deviceList.add(device)
            notifyDataSetChanged()
        }
    }

    fun addDevices(devices: List<OliveWear>) {
        if (!devices.isNotEmpty()) {
            deviceList.addAll(devices)
            notifyDataSetChanged()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            DeviceItemViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_item_device, parent, false))

    override fun onBindViewHolder(holder: DeviceItemViewHolder, position: Int) {
        holder.bind(deviceList[position])
    }

    override fun getItemCount() = deviceList.size

    inner class DeviceItemViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

        val deviceName: AppCompatTextView = itemView.findViewById<AppCompatTextView>(R.id.list_devices_name)
        val deviceMac: AppCompatTextView = itemView.findViewById<AppCompatTextView>(R.id.list_devices_mac)
        val connectButton: AppCompatButton = itemView.findViewById<AppCompatButton>(R.id.list_devices_add)

        fun bind(device: OliveWear) {
            deviceName.text = device.bluetoothDevice.name
            deviceMac.text = device.bluetoothDevice.address

            when (device.bluetoothDevice.bondState) {
                BluetoothDevice.BOND_BONDED -> {
                    if (ProfileUtils.newInstance(view.context).hasOliveDevice(device.bluetoothDevice.address!!)) {
                        connectButton.text = view.context.getString(R.string.button_edit)
                        connectButton.isEnabled = false
                    } else {
                        connectButton.text = view.context.getString(R.string.button_add)
                        connectButton.isEnabled = true
                    }
                }
                BluetoothDevice.BOND_NONE -> {
                    connectButton.text = view.context.getString(R.string.button_pair)
                }
                BluetoothDevice.BOND_BONDING -> {
                    connectButton.text = view.context.getString(R.string.button_pairing)
                    connectButton.isEnabled = false
                }
            }
            connectButton.setOnClickListener {
                onDeviceSelect(device)
            }
        }
    }
}

typealias OnDeviceSelect = (OliveWear) -> Unit
