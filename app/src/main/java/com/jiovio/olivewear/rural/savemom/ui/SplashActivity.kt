package com.jiovio.olivewear.rural.savemom.ui

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.jiovio.olivewear.rural.savemom.util.isLoggedIn
import org.jetbrains.anko.intentFor

/**
 * Created by abara on 14/09/17.
 */
class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (isLoggedIn) {
            startActivity(intentFor<MainActivity>())
        } else {
            startActivity(intentFor<LoginActivity>())
        }
        finish()

    }

}