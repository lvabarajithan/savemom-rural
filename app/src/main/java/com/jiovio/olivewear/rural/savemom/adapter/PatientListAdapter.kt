package com.jiovio.olivewear.rural.savemom.adapter

import android.support.v7.widget.AppCompatImageView
import android.support.v7.widget.AppCompatTextView
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.jiovio.olivehealth.rural.savemom.R
import com.jiovio.olivewear.rural.savemom.api.model.Patient
import org.jetbrains.anko.backgroundColor

/**
 * Created by abara on 23/08/17.
 */
class PatientListAdapter(private val patients: List<Patient>, private val clickAction: (title: String, position: Int) -> Unit) : RecyclerView.Adapter<PatientListAdapter.FeedbackItemHolder>() {

    private var colorsArray: IntArray = IntArray(0)
    private var size = 0

    override fun onBindViewHolder(holder: FeedbackItemHolder, position: Int) {

        val patient = patients[holder.adapterPosition]

        holder.title.text = patient.name
        holder.layout.setOnClickListener {
            clickAction(patient.name!!, holder.adapterPosition)
        }
        holder.card.backgroundColor = colorsArray[holder.adapterPosition % size]
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FeedbackItemHolder {
        val context = parent.context
        colorsArray = context.resources.getIntArray(R.array.feedback_colors)
        size = colorsArray.size
        val view = LayoutInflater.from(context).inflate(R.layout.single_item_patient, parent, false)
        return FeedbackItemHolder(view)
    }

    override fun getItemCount(): Int = patients.size

    class FeedbackItemHolder(view: View) : RecyclerView.ViewHolder(view) {
        val layout = view.findViewById<LinearLayout>(R.id.item_patient_layout)
        val title = view.findViewById<AppCompatTextView>(R.id.item_patient_title)
        val color = view.findViewById<AppCompatImageView>(R.id.item_patient_img)
        val card = view.findViewById<CardView>(R.id.item_patient_card)
    }
}