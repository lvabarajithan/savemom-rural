package com.jiovio.olivewear.rural.savemom.fragment

import android.os.Bundle
import com.jiovio.olivewear.rural.savemom.adapter.CheckupAdapter
import com.jiovio.olivewear.rural.savemom.api.model.Checkup
import kotlinx.android.synthetic.main.fragment_list.*

/**
 * Created by abara on 15/11/17.
 */
class CheckupListFragment : ListFragment() {

    companion object {
        private const val EXTRA_PATIENT_NAME = "patient_name"
        private const val EXTRA_AADHAR_NO = "aadhar_no"
        private const val EXTRA_CHECKUPS = "checkups"
        fun with(patientName: String?, checkups: java.util.ArrayList<Checkup>?, aadharno: String?): CheckupListFragment {
            val fragment = CheckupListFragment()
            val args = Bundle()
            args.putParcelableArrayList(EXTRA_CHECKUPS, checkups)
            args.putString(EXTRA_PATIENT_NAME, patientName)
            args.putString(EXTRA_AADHAR_NO, aadharno)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val patientName = arguments?.getString(EXTRA_PATIENT_NAME) ?: "Unknown"
        val aadharNo = arguments?.getString(EXTRA_AADHAR_NO) ?: "unknown"
        setTitle(patientName)

        val checkups = arguments?.getParcelableArrayList<Checkup>(EXTRA_CHECKUPS) ?: ArrayList()

        fragment_list.adapter = CheckupAdapter(checkups) { lastDate, checkup ->
            nextFragment(CheckupReviewFragment.with(lastDate, checkup, Pair(patientName, aadharNo)))
        }

    }

}