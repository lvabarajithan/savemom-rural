package com.jiovio.olivewear.rural.savemom.mcloud

import android.app.Activity
import android.bluetooth.BluetoothDevice
import android.content.Context
import android.content.IntentFilter
import com.jiovio.olivewear.rural.savemom.data.OliveWear
import com.jiovio.olivewear.rural.savemom.receiver.BluetoothReceiver
import com.jiovio.olivewear.rural.savemom.util.BluetoothUtils

/**
 * Created by abara on 02/09/17.
 */
class DeviceConnectionDelegate(private val deviceName: String, private val onDevicePair: (String) -> Unit) {

    companion object {
        private val EXTRA_DEVICE = "extra_device"

        fun newInstance(deviceName: String, onDevicePair: (String) -> Unit): DeviceConnectionDelegate =
                DeviceConnectionDelegate(deviceName, onDevicePair)
    }

    private var mIsReceiverRegistered = false
    private val mBluetoothReceiver by lazy(LazyThreadSafetyMode.NONE) {
        BluetoothReceiver {
            onDeviceFound(it)
        }
    }

    fun attachReceiver(activity: Activity?) {
        val filter = IntentFilter(BluetoothDevice.ACTION_FOUND)
        filter.addAction(BluetoothDevice.ACTION_PAIRING_REQUEST)
        filter.addAction(BluetoothDevice.ACTION_BOND_STATE_CHANGED)
        activity?.registerReceiver(mBluetoothReceiver, filter)
        mIsReceiverRegistered = true
    }

    private fun onDeviceFound(device: OliveWear) {
        if (device.bluetoothDevice.name == deviceName) {
            if (isDevicePaired(device.bluetoothDevice)) {
                onDevicePair(device.bluetoothDevice.address)
                BluetoothUtils.cancelDiscovery()
            } else {
                pairDevice(device.bluetoothDevice)
            }
        }
    }

    private fun isDevicePaired(device: BluetoothDevice) = device.bondState == BluetoothDevice.BOND_BONDED

    private fun pairDevice(device: BluetoothDevice) {
        device.setPin("0000".toByteArray())
        device.createBond()
    }

    fun detachReceiver(context: Context?) {
        if (mIsReceiverRegistered) {
            context?.unregisterReceiver(mBluetoothReceiver)
        }
    }

}
