package com.jiovio.olivewear.rural.savemom.ui

import android.graphics.Color
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.content.ContextCompat
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import com.jiovio.olivehealth.rural.savemom.R
import com.jiovio.olivewear.rural.savemom.api.model.Checkup
import com.jiovio.olivewear.rural.savemom.util.checkYesOrNo
import kotlinx.android.synthetic.main.activity_symptoms_review.*
import kotlinx.android.synthetic.main.fragment_symptoms_four.*
import kotlinx.android.synthetic.main.fragment_symptoms_one.*
import kotlinx.android.synthetic.main.fragment_symptoms_three.*
import kotlinx.android.synthetic.main.fragment_symptoms_two.*
import org.jetbrains.anko.appcompat.v7.navigationIconResource
import org.jetbrains.anko.backgroundColor

/**
 * Created by abara on 15/11/17.
 */
class SymptomsReviewActivity : AppCompatActivity(), ViewPager.OnPageChangeListener {

    companion object {
        const val EXTRA_CHECKUP = "checkup"
        private lateinit var checkup: Checkup
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_symptoms_review)
        symptoms_appbar.title = "Symptoms"
        symptoms_appbar.setTitleTextColor(Color.WHITE)
        symptoms_appbar.navigationIconResource = R.drawable.ic_close
        setSupportActionBar(symptoms_appbar)

        symptoms_review_viewpager.adapter = SymptomsPagerAdapter(supportFragmentManager)
        symptoms_review_viewpager.addOnPageChangeListener(this)

        onPageSelected(0)

        checkup = intent.getParcelableExtra<Checkup>(EXTRA_CHECKUP)

    }

    override fun onPageSelected(position: Int) {
        symptoms_appbar.backgroundColor = ContextCompat.getColor(this,
                when (position) {
                    0 -> R.color.colorAccentLite
                    1 -> R.color.colorTeal
                    2 -> R.color.colorBlueLight
                    3 -> R.color.colorPink
                    else -> R.color.colorPink
                }
        )
    }

    override fun onPageScrollStateChanged(state: Int) {
        // Nothing
    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
        // Nothing
    }

    class SymptomsPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

        override fun getItem(position: Int): Fragment = when (position) {
            0 -> SymptomsOneFragment()
            1 -> SymptomsTwoFragment()
            2 -> SymptomsThreeFragment()
            3 -> SymptomsFourFragment()
            else -> SymptomsFiveFragment()
        }

        override fun getCount(): Int = 4

    }

    class SymptomsOneFragment : Fragment() {
        override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
            return inflater.inflate(R.layout.fragment_symptoms_one, container, false)
        }

        override fun onActivityCreated(savedInstanceState: Bundle?) {
            super.onActivityCreated(savedInstanceState)
            checkYesOrNo(checkup.symptoms?.headache, fragment_symptoms_headache_yes, fragment_symptoms_headache_no)
            checkYesOrNo(checkup.symptoms?.blurredvision, fragment_symptoms_blurredvision_yes, fragment_symptoms_blurredvision_no)
            checkYesOrNo(checkup.symptoms?.dizziness, fragment_symptoms_dizzy_yes, fragment_symptoms_dizzy_no)
            checkYesOrNo(checkup.symptoms?.shortnessofbreath, fragment_symptoms_breath_yes, fragment_symptoms_breath_no)
            checkYesOrNo(checkup.symptoms?.legedema, fragment_symptoms_edema_yes, fragment_symptoms_edema_no)
            checkYesOrNo(checkup.symptoms?.chestpain, fragment_symptoms_chest_pain_yes, fragment_symptoms_chest_pain_no)
            checkYesOrNo(checkup.symptoms?.abdominalpain, fragment_symptoms_abdominalpain_yes, fragment_symptoms_abdominalpain_no)
        }
    }

    class SymptomsTwoFragment : Fragment() {
        override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
            return inflater.inflate(R.layout.fragment_symptoms_two, container, false)
        }

        override fun onActivityCreated(savedInstanceState: Bundle?) {
            super.onActivityCreated(savedInstanceState)
            checkYesOrNo(checkup.symptoms?.vomiting, fragment_symptoms_nausea_yes, fragment_symptoms_nausea_no)
            checkYesOrNo(checkup.symptoms?.sourtaste, fragment_symptoms_sourtaste_yes, fragment_symptoms_sourtaste_no)
            checkYesOrNo(checkup.symptoms?.stomachburning, fragment_symptoms_burning_yes, fragment_symptoms_burning_no)
            //checkYesOrNo(checkup.symptoms?.spotting, fragment_symptoms_spotting_yes, fragment_symptoms_spotting_no)
            checkYesOrNo(checkup.symptoms?.bleeding, fragment_symptoms_bleeding_yes, fragment_symptoms_bleeding_no)
        }
    }

    class SymptomsThreeFragment : Fragment() {
        override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
            return inflater.inflate(R.layout.fragment_symptoms_three, container, false)
        }

        override fun onActivityCreated(savedInstanceState: Bundle?) {
            super.onActivityCreated(savedInstanceState)
            checkYesOrNo(checkup.symptoms?.urinaryburning, fragment_symptoms_urinary_burning_yes, fragment_symptoms_urinary_burning_no)
            checkYesOrNo(checkup.symptoms?.vaginalitching, fragment_symptoms_vaginal_itching_yes, fragment_symptoms_vaginal_itching_no)
        }
    }

    class SymptomsFourFragment : Fragment() {
        override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
            return inflater.inflate(R.layout.fragment_symptoms_four, container, false)
        }

        override fun onActivityCreated(savedInstanceState: Bundle?) {
            super.onActivityCreated(savedInstanceState)
            checkYesOrNo(checkup.symptoms?.feverandchills, fragment_symptoms_fever_chills_yes, fragment_symptoms_fever_chills_no)
            checkYesOrNo(checkup.symptoms?.rashesonskin, fragment_symptoms_rashes_skin_yes, fragment_symptoms_rashes_skin_no)
            checkYesOrNo(checkup.symptoms?.asthma, fragment_symptoms_asthma_yes, fragment_symptoms_asthma_no)
            checkYesOrNo(checkup.symptoms?.cough, fragment_symptoms_cough_phlegm_yes, fragment_symptoms_cough_phlegm_no)
            checkYesOrNo(checkup.symptoms?.bodypain, fragment_symptoms_body_aches_yes, fragment_symptoms_body_aches_no)
        }
    }

    class SymptomsFiveFragment : Fragment() {
        override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
            return inflater.inflate(R.layout.fragment_symptoms_five, container, false)
        }

        override fun onActivityCreated(savedInstanceState: Bundle?) {
            super.onActivityCreated(savedInstanceState)
            //fragment_symptoms_discomforts.text
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> finish()
        }
        return super.onOptionsItemSelected(item)
    }

}