package com.jiovio.olivewear.rural.savemom.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import com.jiovio.olivehealth.rural.savemom.R
import com.jiovio.olivewear.rural.savemom.api.model.Vitals
import com.jiovio.olivewear.rural.savemom.fragment.measure.MeasureDoneFragment
import com.jiovio.olivewear.rural.savemom.ui.MainActivity
import com.jiovio.olivewear.rural.savemom.ui.MeasureActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_measure.*

/**
 * Created by abara on 16/08/17.
 */
open class BaseFragment : Fragment() {

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        changeTheme()
        super.onActivityCreated(savedInstanceState)
    }

    private fun changeTheme(themeId: Int = R.style.AppTheme) {
        // Default to black status bar
        when (activity) {
            is MainActivity -> (activity as MainActivity).changeTheme(themeId)
            is MeasureActivity -> (activity as MeasureActivity).changeTheme(themeId)
        }
    }

    protected fun nextFragment(fragment: BaseFragment?, backStack: Boolean = true) {
        when (activity) {
            is MainActivity -> (activity as MainActivity).loadFragment(fragment, backStack)
            is MeasureActivity -> (activity as MeasureActivity).loadFragment(fragment, backStack)
        }
    }

    protected fun setTitle(title: String) {
        when (activity) {
            is MainActivity -> (activity as MainActivity).main_appbar_title.text = title
            is MeasureActivity -> (activity as MeasureActivity).measure_appbar_title.text = title
        }
    }

    protected fun setTitle(title: Int) {
        when (activity) {
            is MainActivity -> (activity as MainActivity).main_appbar_title.setText(title)
            is MeasureActivity -> (activity as MeasureActivity).measure_appbar_title.setText(title)
        }
    }

    protected fun toolbarColor(color: Int = R.color.colorPrimary, titleColor: Int = R.color.colorBlack) {
        when (activity) {
            is MainActivity -> (activity as MainActivity).changeToolbarColor(color, titleColor)
            is MeasureActivity -> (activity as MeasureActivity).changeToolbarColor(color, titleColor)
        }
    }

    protected fun getTestTitle(type: String): Int = when (type) {
        MeasureActivity.FragmentType.BP.name -> R.string.measure_title_bp_test_appbar
        MeasureActivity.FragmentType.WEIGHT.name -> R.string.measure_title_weight_test
        MeasureActivity.FragmentType.SPO.name, MeasureDoneFragment.TYPE_FINISH_FRAGMENT -> R.string.measure_title_spo2_test_appbar
        else -> R.string.measure_unknown_test_title
    }

    protected fun getVitals(): Vitals? = when (activity) {
        is MeasureActivity -> (activity as MeasureActivity).getVitals()
        else -> null
    }

}