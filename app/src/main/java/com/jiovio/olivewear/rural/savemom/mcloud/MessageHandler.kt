package com.jiovio.olivewear.rural.savemom.mcloud

import android.os.Handler
import android.os.Message
import android.util.Log
import com.medzone.mcloud.background.BluetoothMessage
import com.medzone.mcloud.background.abHelper.BFactory
import java.util.*

object MessageHandler : Handler() {

    override fun handleMessage(msg: Message) {

        when (msg.what) {
            BluetoothMessage.msg_status -> {
                when (msg.arg2) {
                    BluetoothMessage.msg_device_detected -> {
                        val result = msg.obj as HashMap<*, *>
                        MeasureHelper.mDevAddress = result["detail"] as String
                        MeasureHelper.mRssi = result["rssi"] as Int
                        Log.d("MeasureHelper","device received " + MeasureHelper.mDevAddress)
                        if (!MeasureHelper.mDeviceLists.contains(MeasureHelper.mDevAddress)) {
                            MeasureHelper.mDeviceLists.add(MeasureHelper.mDevAddress)
                            MeasureHelper.mRssiLists.add(MeasureHelper.mRssi as Int)
                        }
                        val subMsg2 = this.obtainMessage(MeasureHelper.DEVICE_DETECTED)
                        subMsg2.obj = MeasureHelper.mDevAddress
                        for (item in MeasureHelper.listeners) {
                            item.handleMessage(subMsg2)
                        }
                    }
                }

                if (BFactory.isChild(MeasureHelper.mType, msg.arg1) || BFactory.isChild(msg.arg1, MeasureHelper.mType)) {
                    val subMsg = this.obtainMessage(MeasureHelper.UPDATE_STATUS)
                    subMsg.arg1 = msg.arg2
                    if (msg.arg2 == BluetoothMessage.msg_socket_connected
                            && (MeasureHelper.mType == 105 || MeasureHelper.mType == 3 || MeasureHelper.mType == 4)) {
                        val result = msg.obj as HashMap<*, *>
                        subMsg.arg2 = result["status"] as Int
                        subMsg.obj = result["detail"]
                    }
                    for (item in MeasureHelper.listeners) {
                        item.handleMessage(subMsg)
                    }
                }
            }
            BluetoothMessage.msg_reply -> {
                val type = msg.arg1
                val result = msg.obj as HashMap<*, *>
                if (MeasureHelper.mType == type) {
                    val subMsg = this.obtainMessage(MeasureHelper.MEASURE_RESULT)
                    subMsg.arg1 = msg.arg2
                    subMsg.arg2 = result["status"] as Int
                    subMsg.obj = result["detail"]

                    for (item in MeasureHelper.listeners) {
                        item.handleMessage(subMsg)
                    }
                }
            }
            else -> {
            }
        }
        super.handleMessage(msg)
    }
}
