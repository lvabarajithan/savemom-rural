package com.jiovio.olivewear.rural.savemom.api

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by abara on 10/09/17.
 */

object ApiClient {

    //private val BASE_URL = "https://savemom-rural-web-api.herokuapp.com/api/"
    private val BASE_URL = "https://savemom-rural.appspot.com/api/"

    val client: Retrofit
        get() = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()

}
