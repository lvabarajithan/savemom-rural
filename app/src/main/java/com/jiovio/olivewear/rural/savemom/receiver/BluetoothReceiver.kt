package com.jiovio.olivewear.rural.savemom.receiver

import android.bluetooth.BluetoothDevice
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.jiovio.olivewear.rural.savemom.data.OliveWear

class BluetoothReceiver(val deviceListener: (OliveWear) -> Unit)
    : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {

        when (intent.action) {
            BluetoothDevice.ACTION_FOUND,
            BluetoothDevice.ACTION_BOND_STATE_CHANGED,
            BluetoothDevice.ACTION_PAIRING_REQUEST -> {
                val bluetoothDevice =
                        intent.getParcelableExtra<BluetoothDevice>(BluetoothDevice.EXTRA_DEVICE)
                deviceListener(OliveWear(bluetoothDevice))
            }
        }
    }
}
