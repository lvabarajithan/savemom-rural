package com.jiovio.olivewear.rural.savemom.ui

import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.*
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import com.jiovio.olivehealth.rural.savemom.R
import com.jiovio.olivewear.rural.savemom.api.model.Data
import com.jiovio.olivewear.rural.savemom.api.model.Prescription
import com.jiovio.olivewear.rural.savemom.ui.PrescriptionDataActivity.Companion.EXTRA_PRESCRIPTION_DATA
import kotlinx.android.synthetic.main.activity_prescription.*
import org.jetbrains.anko.find
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast

/**
 * Created by abara on 16/11/17.
 */
class PrescriptionActivity : AppCompatActivity() {

    companion object {
        const val EXTRA_PRESCRIPTIONS = "prescriptions"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_prescription)
        prescription_appbar.title = "Feedback"
        setSupportActionBar(prescription_appbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val prescriptions = intent.getParcelableArrayListExtra<Prescription>(EXTRA_PRESCRIPTIONS)

        prescription_list.itemAnimator = DefaultItemAnimator()
        prescription_list.layoutManager = LinearLayoutManager(this)
        prescription_list.setHasFixedSize(true)

        prescription_list.adapter = object : RecyclerView.Adapter<PrescriptionHolder>() {

            override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PrescriptionHolder {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.single_item_prescription, parent, false)
                return PrescriptionHolder(view)
            }

            override fun onBindViewHolder(holder: PrescriptionHolder, position: Int) {

                val pre = prescriptions[holder.adapterPosition]
                holder.title.text = "${pre.role} - ${pre.prescriptiondate} ${pre.prescriptiontime}"
                holder.desc.text = "Name: ${pre.doctorname}\nMobile: ${pre.mobile}"
                holder.layout.setOnClickListener { clickAction(pre.data) }
                holder.detailsBtn.setOnClickListener { clickAction(pre.data) }

            }

            override fun getItemCount(): Int = prescriptions.size

        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> finish()
        }
        return super.onOptionsItemSelected(item)
    }

    private val clickAction = { data: ArrayList<Data>? ->
        if (data != null) {
            startActivity<PrescriptionDataActivity>(EXTRA_PRESCRIPTION_DATA to data)
        } else {
            toast("No prescriptions provided!")
        }
    }


    class PrescriptionHolder(view: View) : RecyclerView.ViewHolder(view) {

        val title = view.find<AppCompatTextView>(R.id.item_prescription_title)
        val desc = view.find<AppCompatTextView>(R.id.item_prescription_desc)
        val layout = view.find<ConstraintLayout>(R.id.item_prescription_layout)
        val detailsBtn = view.find<AppCompatButton>(R.id.item_prescription_details_btn)

    }

}