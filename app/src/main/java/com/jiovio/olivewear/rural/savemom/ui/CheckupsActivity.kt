package com.jiovio.olivewear.rural.savemom.ui

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jiovio.olivehealth.rural.savemom.R
import com.jiovio.olivewear.rural.savemom.api.model.Nutrition
import com.jiovio.olivewear.rural.savemom.api.model.Reminder
import com.jiovio.olivewear.rural.savemom.api.model.Supply
import com.jiovio.olivewear.rural.savemom.api.model.Symptoms
import com.jiovio.olivewear.rural.savemom.fragment.BaseFragment
import com.jiovio.olivewear.rural.savemom.fragment.PatientsCheckupFragment
import com.jiovio.olivewear.rural.savemom.util.commitTransaction
import kotlinx.android.synthetic.main.activity_checkups.*
import kotlinx.android.synthetic.main.fragment_checkups.*
import org.jetbrains.anko.imageResource

/**
 * Created by abara on 03/11/17.
 */
class CheckupsActivity : BaseActivity() {

    companion object {
        const val EXTRA_TYPE = "checkup_type"
        const val EXTRA_TITLE = "checkup_title"
        private const val TYPE_NUTRITION = 1
        private const val TYPE_SYMPTOMS = 2
        private const val TYPE_SUPPLY = 3
        private const val TYPE_REMINDER = 5

        private var currentPosition = 0

        private val symptoms = arrayOf(
                R.string.symptoms_blurredvision to R.drawable.ic_symptom_blurred,
                R.string.symptoms_dizziness to R.drawable.ic_symptom_dizziness,
                R.string.symptoms_shortness_breath to R.drawable.ic_symptom_breath,
                R.string.symptoms_legedema to R.drawable.ic_symptom_legedema,
                R.string.symptoms_chestpain to R.drawable.ic_symptom_chest,
                R.string.symptoms_abdominalpain to R.drawable.ic_symptom_abdominal,
                R.string.symptoms_vomiting to R.drawable.ic_symptom_vomiting,
                R.string.symptoms_stomach_burning to R.drawable.ic_symptom_stomachburning,
                R.string.symptoms_urinary_burning to R.drawable.ic_symptom_urinaryburning,
                R.string.symptoms_bleeding to R.drawable.ic_symptom_bleeding,
                R.string.symptoms_feverandchills to R.drawable.ic_symptom_feverandchills,
                R.string.symptoms_rashesonskin to R.drawable.ic_symptom_rashesonskin,
                R.string.symptoms_vaginalitching to R.drawable.ic_symptom_vaginalitching,
                R.string.symptoms_asthma to R.drawable.ic_symptom_asthma,
                R.string.symptoms_bodypain to R.drawable.ic_symptom_bodypain,
                R.string.symptoms_cough to R.drawable.ic_symptom_cough,
                R.string.symptoms_sourtaste to R.drawable.ic_symptom_sourtaste,
                R.string.symptoms_headache to R.drawable.ic_symptom_headache
        )

        private val supply = arrayOf(
                R.string.supply_nutritionsuppliments to R.drawable.ic_supply_nutritionsuppliments,
                R.string.supply_pholicacidtablet to R.drawable.ic_supply_pholicacidtablet,
                R.string.supply_informquantity to R.drawable.ic_supply_informquantity
        )

        private val nutrition = arrayOf(
                R.string.nutrition_regularantenatalvisit to R.drawable.ic_nutrition,
                R.string.nutrition_stressofwork to R.drawable.ic_nutrition,
                R.string.nutrition_takingmedicines to R.drawable.ic_nutrition,
                R.string.nutrition_takingwater to R.drawable.ic_nutrition,
                R.string.nutrition_eatingprotienfood to R.drawable.ic_nutrition,
                R.string.nutrition_happywithfamily to R.drawable.ic_nutrition,
                R.string.nutrition_comsumingtobbacco to R.drawable.ic_nutrition,
                R.string.nutrition_consumingcoffee to R.drawable.ic_nutrition
        )

        private val reminder = arrayOf(R.string.reminder_next_group_session to R.drawable.ic_reminder_next_group_session)

        private lateinit var currType: Array<Pair<Int, Int>>
    }

    private var typeVal: Int = 0
    private var currTypeSize = symptoms.size
    private lateinit var checkupData: Parcelable

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_checkups)
        checkups_appbar.title = intent.getStringExtra(EXTRA_TITLE)
        setSupportActionBar(checkups_appbar)

        typeVal = intent.getIntExtra(EXTRA_TYPE, TYPE_SYMPTOMS)

        currentPosition = 0
        currType = when (typeVal) {
            TYPE_REMINDER -> {
                checkupData = Reminder()
                reminder
            }
            TYPE_SUPPLY -> {
                checkupData = Supply()
                supply
            }
            TYPE_NUTRITION -> {
                checkupData = Nutrition()
                nutrition
            }
            else -> {
                checkupData = Symptoms()
                symptoms
            }
        }
        currTypeSize = currType.size
        loadFragment(CheckupsFragment(), false)

    }

    fun buttonClick(value: String) {

        val shouldLoad = when (currType) {
            reminder -> buildReminder(value, checkupData as Reminder)
            supply -> buildSupply(value, checkupData as Supply)
            nutrition -> buildNutrition(value, checkupData as Nutrition)
            else -> buildSymptoms(value, checkupData as Symptoms)
        }
        if (shouldLoad) {
            currentPosition++
            loadFragment(CheckupsFragment())
        } else {
            val data = Intent()
            data.putExtra(PatientsCheckupFragment.EXTRA_RESULT, checkupData)
            data.putExtra(EXTRA_TYPE, typeVal)
            setResult(Activity.RESULT_OK, data)
            finish()
        }
    }

    private fun buildSymptoms(value: String, symptoms: Symptoms): Boolean {
        if (currentPosition == currType.size - 1) return false

        when (currentPosition) {
            0 -> symptoms.blurredvision = value
            1 -> symptoms.dizziness = value
            2 -> symptoms.shortnessofbreath = value
            3 -> symptoms.legedema = value
            4 -> symptoms.chestpain = value
            5 -> symptoms.abdominalpain = value
            6 -> symptoms.vomiting = value
            7 -> symptoms.stomachburning = value
            8 -> symptoms.urinaryburning = value
            9 -> symptoms.bleeding = value
            10 -> symptoms.feverandchills = value
            11 -> symptoms.rashesonskin = value
            12 -> symptoms.vaginalitching = value
            13 -> symptoms.asthma = value
            14 -> symptoms.bodypain = value
            15 -> symptoms.cough = value
            16 -> symptoms.sourtaste = value
            17 -> symptoms.headache = value
        }
        return true
    }

    private fun buildNutrition(value: String, nutrition: Nutrition): Boolean {
        if (currentPosition == currType.size - 1) return false

        when (currentPosition) {
            0 -> nutrition.regularantenatalvisit = value
            1 -> nutrition.stressofwork = value
            2 -> nutrition.takingmedicines = value
            3 -> nutrition.takingwater = value
            4 -> nutrition.eatingprotienfood = value
            5 -> nutrition.happywithfamily = value
            6 -> nutrition.comsumingtobbacco = value
            7 -> nutrition.consumingcoffee = value
        }
        return true
    }

    private fun buildSupply(value: String, supply: Supply): Boolean {
        if (currentPosition == currType.size - 1) return false

        when (currentPosition) {
            0 -> supply.nutritionsuppliments = value
            1 -> supply.pholicacidtablet = value
            2 -> supply.informquantity = value
        }
        return true
    }

    private fun buildReminder(value: String, checkupData: Reminder): Boolean {
        checkupData.value = value
        return false
    }

    class CheckupsFragment : BaseFragment() {

        override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
            return inflater.inflate(R.layout.fragment_checkups, container, false)
        }

        override fun onActivityCreated(savedInstanceState: Bundle?) {
            super.onActivityCreated(savedInstanceState)

            checkup_confirm_text.text = resources.getString(currType[currentPosition].first)
            checkup_confirm_image.imageResource = currType[currentPosition].second

            checkup_confirm_no.setOnClickListener {
                (activity as CheckupsActivity).buttonClick(resources.getString(R.string.checkup_confirm_no).toLowerCase())
            }
            checkup_confirm_yes.setOnClickListener {
                (activity as CheckupsActivity).buttonClick(resources.getString(R.string.checkup_confirm_yes).toLowerCase())
            }

        }

    }

    override fun onBackPressed() {
        super.onBackPressed()
        if (currentPosition > 0) currentPosition--
    }

    override fun changeToolbarColor(color: Int, titleColor: Int) {
        // Do nothing
    }

    override fun loadFragment(fragment: BaseFragment?, addToBackStack: Boolean) {
        fragment?.let {
            supportFragmentManager.commitTransaction {
                if (addToBackStack) addToBackStack(null)
                replace(R.id.checkups_content, fragment)
            }
        }
    }

}