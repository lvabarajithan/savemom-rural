package com.jiovio.olivewear.rural.savemom.util

import android.content.Context

/**
 * Created by abara on 02/09/17.
 */
class ProfileUtils private constructor(context: Context?) {

    private val mPreferenceUtils: PreferenceUtils = PreferenceUtils.newInstance(context)

    enum class DeviceType {
        BP,
        BG,
        SPO,
        TEMPERATURE,
        WEIGHT,
    }

    enum class MeasureType {
        AUTO,
        MANUAL,
    }

    val isLoggedIn: Boolean
        get() = mPreferenceUtils.getBooleanPrefs(PREF_IS_LOGGED_IN)

    fun setLoggedIn() {
        mPreferenceUtils[PREF_IS_LOGGED_IN] = true
    }

    fun logout() {
        mPreferenceUtils.clear()
    }

    fun setDevice(selectedOrdinal: Int, address: String?) {
        setDevice(ProfileUtils.DeviceType.values()[selectedOrdinal].name, address!!)
    }

    fun setDevice(deviceType: String, address: String) {
        mPreferenceUtils["device_address_$deviceType"] = address
        val existingDevices = mPreferenceUtils.getStringSet("all_devices")
        if (address !in existingDevices) {
            existingDevices.toMutableSet().add(address)
        }
        mPreferenceUtils.setStringSet("all_devices", existingDevices)
    }

    fun setMeasureAuto(measureType: MeasureType) {
        if (measureType == MeasureType.AUTO) {
            mPreferenceUtils["measure_auto"] = true
        }
    }

    fun hasOliveDevices() = mPreferenceUtils.getBooleanPrefs("has_olive_device")

    fun hasOliveDeviceForType(type: String)
            = mPreferenceUtils.getStringPrefs("device_address_$type").isNotEmpty()

    fun hasOliveDevice(deviceAddress: String) = deviceAddress in mPreferenceUtils.getStringSet("all_devices")

    fun getDeviceAddress(type: String): String = mPreferenceUtils.getStringPrefs("device_address_$type")

    companion object {
        private val PREF_IS_LOGGED_IN = "pref_is_logged_in"

        fun newInstance(context: Context?): ProfileUtils {
            return ProfileUtils(context)
        }
    }
}
