package com.jiovio.olivewear.rural.savemom.ui

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.AppCompatTextView
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import com.jiovio.olivehealth.rural.savemom.R
import com.jiovio.olivewear.rural.savemom.api.model.Data
import kotlinx.android.synthetic.main.activity_prescription.*
import org.jetbrains.anko.find

/**
 * Created by abara on 16/11/17.
 */
class PrescriptionDataActivity : AppCompatActivity() {

    companion object {
        const val EXTRA_PRESCRIPTION_DATA = "prescription_data"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_prescription)
        prescription_appbar.title = "Prescriptions"
        setSupportActionBar(prescription_appbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val datum = intent.getParcelableArrayListExtra<Data>(EXTRA_PRESCRIPTION_DATA)

        prescription_list.itemAnimator = DefaultItemAnimator()
        prescription_list.layoutManager = LinearLayoutManager(this)
        prescription_list.setHasFixedSize(true)

        prescription_list.adapter = object : RecyclerView.Adapter<PrescriptionDataHolder>() {

            override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PrescriptionDataHolder {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.single_item_prescription, parent, false)
                return PrescriptionDataHolder(view)
            }

            override fun onBindViewHolder(holder: PrescriptionDataHolder, position: Int) {

                val data = datum[holder.adapterPosition]
                holder.title.text = "${data.tabletname}"
                holder.desc.text = "Dosage: ${data.dosage}\n" +
                        "Morning (${data.morningfood}): ${data.morning}\n" +
                        "Night (${data.nightfood}): ${data.night}"

            }

            override fun getItemCount(): Int = datum.size

        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            android.R.id.home->finish()
        }
        return super.onOptionsItemSelected(item)
    }

    class PrescriptionDataHolder(view: View) : RecyclerView.ViewHolder(view) {

        val title = view.find<AppCompatTextView>(R.id.item_prescription_data_title)
        val desc = view.find<AppCompatTextView>(R.id.item_prescription_data_desc)

    }
}