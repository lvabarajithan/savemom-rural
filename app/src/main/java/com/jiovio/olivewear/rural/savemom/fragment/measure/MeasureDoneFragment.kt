package com.jiovio.olivewear.rural.savemom.fragment.measure

import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jiovio.olivehealth.rural.savemom.R
import com.jiovio.olivewear.rural.savemom.fragment.BaseFragment
import com.jiovio.olivewear.rural.savemom.ui.MeasureActivity
import kotlinx.android.synthetic.main.activity_measure.*
import kotlinx.android.synthetic.main.fragment_measure_done.*
import kotlinx.android.synthetic.main.layout_bmi_results.*
import kotlinx.android.synthetic.main.layout_bmi_results2.*
import kotlinx.android.synthetic.main.layout_bp_results.*
import kotlinx.android.synthetic.main.layout_spo_results.*
import org.jetbrains.anko.backgroundColor
import org.jetbrains.anko.backgroundDrawable
import org.jetbrains.anko.support.v4.find
import org.jetbrains.anko.support.v4.toast

/**
 * Created by abara on 23/10/17.
 */
class MeasureDoneFragment : BaseFragment() {

    companion object {
        private const val EXTRA_BP_RESULT = "extra_result"
        private const val EXTRA_PAIR_RESULT = "extra_pair_result"
        const val TYPE_FINISH_FRAGMENT = "finish_fragment"
        private val EXTRA_TYPE = MeasureActivity.FragmentType.BP.name
        fun getInstance(type: String, bpResult: String? = null, weightOrSpoPair: Pair<*, *>? = null): MeasureDoneFragment {
            val fragment = MeasureDoneFragment()
            val args = Bundle()
            args.putString(EXTRA_TYPE, type)
            bpResult?.let { args.putString(EXTRA_BP_RESULT, bpResult) }
            weightOrSpoPair?.let { args.putSerializable(EXTRA_PAIR_RESULT, weightOrSpoPair) }
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_measure_done, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        toolbarColor()

        val type = arguments!!.getString(EXTRA_TYPE)

        setTitle(getTestTitle(type))

        setFinishButton {
            val finishFragment = getNextFragmentType(type)
            if (finishFragment != TYPE_FINISH_FRAGMENT) {
                nextFragment(MeasureIntroFragment.getInstance(finishFragment))
            } else {
                // show Pregnancy weight gain result
                val pairs = arguments!!.getSerializable(EXTRA_PAIR_RESULT) as Pair<Pair<Int, Int>, Pair<Int, Int>>
                nextFragment(getInstance(TYPE_FINISH_FRAGMENT, weightOrSpoPair = pairs.second))
            }
        }

        when (type) {
            MeasureActivity.FragmentType.BP.name -> {
                val view = find<View>(R.id.bp_results_layout)
                view.visibility = View.VISIBLE
                val result = arguments!!.getString(EXTRA_BP_RESULT)
                bp_done_sys_value.text = resources.getString(R.string.bp_sys_value, 0)
                bp_done_dia_value.text = resources.getString(R.string.bp_dia_value, 0)
                bp_done_pulse_value.text = resources.getString(R.string.bp_pulse_value, 0)
                toast(result)
            }
            MeasureActivity.FragmentType.WEIGHT.name -> {
                val view = find<View>(R.id.bmi_results_layout)
                view.visibility = View.VISIBLE

                val pairs = arguments!!.getSerializable(EXTRA_PAIR_RESULT) as Pair<Pair<Int, Int>, Pair<Int, Int>>
                val pair = pairs.first

                bmi_results_weight_value.text = resources.getString(R.string.bmi_results_weight_value, pair.first)
                bmi_results_value.text = resources.getString(R.string.bmi_results_value, pair.second)

                val bmiResult = getBmiResult(pair.second)

                measure_done_condition.text = resources.getString(bmiResult.first)
                measure_done_round_img.backgroundDrawable = ContextCompat.getDrawable(activity!!, bmiResult.second)

            }
            MeasureActivity.FragmentType.SPO.name -> {
                val view = find<View>(R.id.spo_results_layout)
                view.visibility = View.VISIBLE
                val pair = arguments!!.getSerializable(EXTRA_PAIR_RESULT) as Pair<Int, Int>
                spo_result_value.text = resources.getString(R.string.spo2_value, pair.first)
                spo_result_pulse.text = resources.getString(R.string.spo2_pulse_value, pair.second)
            }
            TYPE_FINISH_FRAGMENT -> {

                val view = find<View>(R.id.bmi_results2_layout)
                view.visibility = View.VISIBLE
                setFinishButton {
                    (activity as MeasureActivity).vitalsMeasureDone()
                }

                val pair = arguments!!.getSerializable(EXTRA_PAIR_RESULT) as Pair<Int, Int>

                bmi_results2_weight_value.text = resources.getString(R.string.bmi_results_weight_value, pair.first)
                bmi_results2_value.text = resources.getString(R.string.bmi_results2_value, pair.second)

                val weightGainResult = getWeightGainResult(pair.second)

                measure_done_condition.text = resources.getString(weightGainResult.first)
                measure_done_round_img.backgroundDrawable = ContextCompat.getDrawable(activity!!, weightGainResult.second)

            }
        }

    }

    private fun setFinishButton(clickListener: () -> Unit) {
        val finishBtn = (activity)!!.measure_btn
        finishBtn.text = "Finish"
        finishBtn.backgroundColor = ContextCompat.getColor(activity!!, R.color.colorTeal)
        finishBtn.setOnClickListener { clickListener() }
    }

    private fun getWeightGainResult(gainedWeight: Int): Pair<Int, Int> = if (gainedWeight >= 2) {
        Pair(R.string.bmi_condition_abnormal, R.drawable.ic_round_red)
    } else {
        Pair(R.string.bmi_condition_normalweight, R.drawable.ic_round_teal)
    }

    private fun getBmiResult(bmi: Int): Pair<Int, Int> = when (bmi) {
        in 0..19 -> Pair(R.string.bmi_condition_underweight, R.drawable.ic_round_red)
        in 19..25 -> Pair(R.string.bmi_condition_normalweight, R.drawable.ic_round_green)
        in 25..30 -> Pair(R.string.bmi_condition_over_weight, R.drawable.ic_round_yellow)
        else -> Pair(R.string.bmi_condition_obese, R.drawable.ic_round_red)
    }

    private fun getNextFragmentType(type: String): String = when (type) {
        MeasureActivity.FragmentType.BP.name -> MeasureActivity.FragmentType.SPO.name
        MeasureActivity.FragmentType.SPO.name -> MeasureActivity.FragmentType.WEIGHT.name
        else -> TYPE_FINISH_FRAGMENT
    }

}