package com.jiovio.olivewear.rural.savemom.fragment

import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jiovio.olivehealth.rural.savemom.R
import com.jiovio.olivewear.rural.savemom.adapter.CheckupAdapter
import com.jiovio.olivewear.rural.savemom.api.model.Checkup
import com.jiovio.olivewear.rural.savemom.ui.*
import com.jiovio.olivewear.rural.savemom.ui.PrescriptionActivity.Companion.EXTRA_PRESCRIPTIONS
import com.jiovio.olivewear.rural.savemom.ui.PrescriptionDataActivity.Companion.EXTRA_PRESCRIPTION_DATA
import com.jiovio.olivewear.rural.savemom.ui.VitalsSummaryActivity.Companion.EXTRA_AADHAR_NO
import com.jiovio.olivewear.rural.savemom.ui.VitalsSummaryActivity.Companion.EXTRA_PATIENT_NAME
import com.jiovio.olivewear.rural.savemom.ui.VitalsSummaryActivity.Companion.EXTRA_VITALS
import kotlinx.android.synthetic.main.fragment_list.*
import org.jetbrains.anko.bundleOf
import org.jetbrains.anko.support.v4.startActivity
import org.jetbrains.anko.support.v4.toast

/**
 * Created by abara on 15/11/17.
 */
class CheckupReviewFragment : ListFragment() {

    companion object {
        private const val EXTRA_CHECKUP = "checkup"
        private const val EXTRA_LAST_DATE = "last_date"
        private const val EXTRA_NAME_AADHAR_PAIR = "name_aadhar_pair"
        fun with(lastDate: String, checkup: Checkup, nameAadharPair: Pair<String, String>): CheckupReviewFragment {
            val fragment = CheckupReviewFragment()
            fragment.arguments = bundleOf(EXTRA_CHECKUP to checkup,
                    EXTRA_LAST_DATE to lastDate, EXTRA_NAME_AADHAR_PAIR to nameAadharPair)
            return fragment
        }
    }

    private var checkup: Checkup? = null

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        checkup = arguments?.getParcelable<Checkup>(EXTRA_CHECKUP)
        val lastDate = arguments?.getString(EXTRA_LAST_DATE)

        setTitle("Test on ${checkup?.date ?: "unknown date"}")

        val titles = resources.getStringArray(R.array.patient_checkup_review_items)

        fragment_list.adapter = object : RecyclerView.Adapter<CheckupAdapter.CheckupHolder>() {
            override fun getItemCount(): Int = titles.size

            override fun onBindViewHolder(holder: CheckupAdapter.CheckupHolder, position: Int) {
                holder.color.visibility = View.GONE
                holder.title.text = titles[position]
                holder.desc.text = lastDate
                holder.layout.setOnClickListener {
                    clickAction(holder.adapterPosition)
                }
            }

            override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CheckupAdapter.CheckupHolder {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.single_item_checkup_list, parent, false)
                return CheckupAdapter.CheckupHolder(view)
            }

        }

    }

    private val clickAction = { pos: Int ->

        if (checkup != null) {
            when (pos) {
                0 -> { // feedback
                    val pre = checkup!!.prescription
                    if (pre != null) {
                        startActivity<PrescriptionActivity>(EXTRA_PRESCRIPTIONS to pre)
                    } else {
                        toast("No Feedbacks!")
                    }
                }
                1 -> {
                    // prescription
                    val pre = checkup!!.prescription
                    if (pre != null) {
                        val data = pre[0].data
                        if (data != null) {
                            startActivity<PrescriptionDataActivity>(EXTRA_PRESCRIPTION_DATA to data)
                        } else {
                            toast("No prescriptions!")
                        }
                    } else {
                        toast("No prescriptions!")
                    }
                }
                2 -> {
                    // reports
                    if (checkup!!.reports != null) {
                        startActivity<ReportsActivity>(ReportsActivity.EXTRA_REPORTS to checkup!!.reports!!, ReportsActivity.EXTRA_ENABLE_ADD to false)
                    } else {
                        toast("No reports found!")
                    }
                }
                3 -> {
                    // symptoms
                    startActivity<SymptomsReviewActivity>(SymptomsReviewActivity.EXTRA_CHECKUP to checkup!!)
                    toast("Loading symptoms...")
                }
                4 -> {
                    // vitals
                    val vitals = checkup!!.vitals
                    if (vitals != null) {
                        val nameAadharPair = arguments?.getSerializable(EXTRA_NAME_AADHAR_PAIR) as Pair<String, String>
                        startActivity<VitalsSummaryActivity>(EXTRA_PATIENT_NAME to nameAadharPair.first,
                                EXTRA_AADHAR_NO to nameAadharPair.second, EXTRA_VITALS to vitals, EXTRA_LAST_DATE to (arguments?.getString(EXTRA_LAST_DATE) ?: "unknown"))
                    } else {
                        toast("No vitals test done!")
                    }
                }
                5 -> {
                    // generic info
                    // not reachable
                }
            }
        } else {
            toast("Invalid checkup details!")
        }

    }

}