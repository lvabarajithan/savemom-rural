package com.jiovio.olivewear.rural.savemom.ui

import android.Manifest
import android.app.Activity
import android.app.DatePickerDialog
import android.app.ProgressDialog
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.provider.MediaStore
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.DatePicker
import com.jiovio.olivehealth.rural.savemom.R
import com.jiovio.olivewear.rural.savemom.api.ApiClient
import com.jiovio.olivewear.rural.savemom.api.ApiService
import com.jiovio.olivewear.rural.savemom.api.model.NewPatient
import com.jiovio.olivewear.rural.savemom.api.model.Patient
import com.jiovio.olivewear.rural.savemom.api.model.Village
import com.jiovio.olivewear.rural.savemom.util.*
import kotlinx.android.synthetic.main.activity_newuser.*
import kotlinx.android.synthetic.main.fragment_family_history.*
import kotlinx.android.synthetic.main.fragment_general_health.*
import kotlinx.android.synthetic.main.fragment_medical_history.*
import kotlinx.android.synthetic.main.fragment_new_user_aadhar.*
import kotlinx.android.synthetic.main.fragment_partner_details.*
import kotlinx.android.synthetic.main.fragment_personal_details.*
import kotlinx.android.synthetic.main.fragment_previous_pregnancy.*
import kotlinx.android.synthetic.main.fragment_previous_pregnancy_xp.*
import org.jetbrains.anko.*
import org.jetbrains.anko.appcompat.v7.navigationIconResource
import org.jetbrains.anko.appcompat.v7.titleResource
import org.jetbrains.anko.support.v4.indeterminateProgressDialog
import org.jetbrains.anko.support.v4.toast
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

/**
 * Created by abara on 01/11/17.
 */
class NewUserActivity : AppCompatActivity(), View.OnClickListener, Callback<NewPatient> {

    private lateinit var dialog: ProgressDialog

    private val LAST_POSITION = "last_position"
    private var fragmentPos: Int = 0

    private companion object {
        const val RC_IMAGE_CAPTURE = 98
        const val RC_IMAGE_CAPTURE_AADHAR = 99
    }

    private var newPatient: Patient = Patient()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_newuser)
        newuser_appbar.navigationIconResource = R.drawable.ic_close
        newuser_appbar.title = ""
        setSupportActionBar(newuser_appbar)

        fragmentPos = savedInstanceState?.getInt(LAST_POSITION) ?: 0
        nextFragment(fragmentPos == 0)
        newuser_next_btn.setOnClickListener(this)
        newuser_back_btn.visibility = View.GONE

        newuser_back_btn.setOnClickListener {
            onBackPressed()
        }

    }

    override fun onResponse(call: Call<NewPatient>?, response: Response<NewPatient>?) {
        if (response != null) {
            val resp = response.body()
            if (resp?._id != null) {
                toast("User created!")
                finish()
            } else {
                toast("Cannot create new user.\nTry again.")
            }
        } else {
            toast("Cannot create new user.\nTry again.")
        }
        dialog.dismiss()
    }

    override fun onFailure(call: Call<NewPatient>?, t: Throwable?) {
        toast("Cannot create new user.\nTry again.")
        dialog.dismiss()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt(LAST_POSITION, fragmentPos)
    }

    private fun nextFragment(firstFragment: Boolean = false) {

        changeTheme()
        supportFragmentManager.commitTransaction {
            if (!firstFragment) addToBackStack(null)
            replace(R.id.newuser_content, NewUserFragment.load(fragmentPos))
        }

    }

    private fun changeTheme() {
        val pair = getTitleAndColor()
        val colorTheme = ContextCompat.getColor(this, pair.second)
        newuser_appbar.titleResource = pair.first
        newuser_appbar.backgroundColor = colorTheme
        newuser_next_btn.textColor = colorTheme
        newuser_back_btn.textColor = colorTheme

        val visibility = when (fragmentPos) {
            0 -> View.GONE
            7 -> View.GONE
            else -> View.VISIBLE
        }
        newuser_back_btn.visibility = visibility

        val nextBtnText = when (fragmentPos) {
            7 -> R.string.newuser_finish_button
            else -> R.string.newuser_next_button
        }
        newuser_next_btn.textResource = nextBtnText

    }

    fun performCameraAction(granted: Boolean, requestCode: Int) {
        if (granted) {

            val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            if (intent.resolveActivity(packageManager) != null) {
                startActivityForResult(intent, requestCode)
            }

        } else {
            toast("Permission required to access camera")
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if ((requestCode == RC_IMAGE_CAPTURE) and (resultCode == Activity.RESULT_OK)) {

            data?.let {
                val extras = data.extras
                val bitmapImage = extras.get("data") as Bitmap
                newPatient.image = bitmapImage.asBase64String()
                toast("Patient image added!")
            }

        }

        if ((requestCode == RC_IMAGE_CAPTURE_AADHAR) and (resultCode == Activity.RESULT_OK)) {

            data?.let {
                val extras = data.extras
                val bitmapImage = extras.get("data") as Bitmap
                newPatient.aadharimg = bitmapImage.asBase64String()
                toast("Patient aadhar image added!")
            }

        }

    }

    override fun onClick(view: View) {

        when (fragmentPos) {
            0 -> {
                val aadharNo = fragment_new_user_aadhar_box.text.toString()
                if (aadharNo.isNotEmpty()) {
                    newPatient.aadharno = aadharNo
                } else {
                    toast("Enter Aadhar number")
                    return
                }
            }
            1 -> {

                val name = newuser_personalinfo_name.text.toString()
                val fatherName = newuser_personalinfo_father_name.text.toString()
                val husbandName = newuser_personalinfo_husband_name.text.toString()
                val village = newuser_personalinfo_colony_name.text.toString()
                val district = newuser_personalinfo_district_name.text.toString()
                val state = newuser_personalinfo_state_name.text.toString()
                val pincode = newuser_personalinfo_pincode_name.text.toString()
                val age = newuser_personalinfo_age_name.text.toString()
                val duedate = newuser_personalinfo_duedate_name.text.toString()
                val phno = newuser_personalinfo_phone_number.text.toString()
                if (name.isNotEmpty() and fatherName.isNotEmpty() and husbandName.isNotEmpty() and
                        village.isNotEmpty() and district.isNotEmpty() and state.isNotEmpty() and
                        pincode.isNotEmpty() and age.isNotEmpty() and duedate.isNotEmpty() and phno.isNotEmpty()) {
                    newPatient.name = name
                    newPatient.fathername = fatherName
                    newPatient.husbandname = husbandName
                    newPatient.villagename = village
                    newPatient.district = district
                    newPatient.state = state
                    newPatient.pincode = pincode
                    newPatient.age = age
                    newPatient.duedate = duedate
                    newPatient.phoneno = phno
                } else {
                    toast("Fill all details")
                    return
                }
            }
            2 -> {
                newPatient.bp = fragment_gh_bp_yes.getCheckedString()
                newPatient.diabetics = fragment_gh_diabetics_yes.getCheckedString()
                newPatient.anemia = fragment_gh_anemia_yes.getCheckedString()
                newPatient.tobacco = fragment_gh_tobacco_yes.getCheckedString()
                newPatient.hiv = fragment_gh_hiv_yes.getCheckedString()
                newPatient.cancer = fragment_gh_cancer_yes.getCheckedString()
                newPatient.tb = fragment_gh_tb_yes.getCheckedString()
            }
            3 -> {
                newPatient.firstpregnancy = fragment_medical_history_fp_yes.getCheckedString()
                val months = fragment_medical_history_months_box.text.toString()
                val children = fragment_medical_history_children_box.text.toString()
                val age = fragment_medical_history_age_box.text.toString()
                if (months.isNotEmpty() and children.isNotEmpty() and age.isNotEmpty()) {
                    newPatient.howmanymonths = months
                    newPatient.howmanychildren = months
                    newPatient.ageofchildern = months
                } else {
                    toast("Enter all details")
                    return
                }
            }
            4 -> {
                newPatient.abortion = fragment_pp_abortion_yes.getCheckedString()
                newPatient.stillbirth = fragment_pp_stillbirth_yes.getCheckedString()
                newPatient.undernourished = fragment_pp_habit_nourished.isChecked
                newPatient.lowvitamin = fragment_pp_habit_vitamin.isChecked
                newPatient.lowprotien = fragment_pp_habit_protein.isChecked
                newPatient.lowcalcium = fragment_pp_habit_calcium.isChecked
                newPatient.work = fragment_pp_work_nw.getCheckedString(R.string.fragment_pp_work_manual)
            }
            5 -> {
                newPatient.previousantenatal = fragment_prev_pregxp_antenatal_yes.getCheckedString()
                newPatient.takemedicineandvitamins = fragment_prev_pregxp_med_vit_yes.getCheckedString()
                val checkups = fragment_prev_pregxp_count_antenatal_box.text.toString()
                if (checkups.isNotEmpty()) {
                    newPatient.howmanyantenatal = checkups
                } else {
                    toast("Enter Antenatal checkup count")
                    return
                }
            }
            6 -> {
                newPatient.husbandworking = fragment_partner_details_husband_working_yes.getCheckedString()
                newPatient.drinking = fragment_partner_details_habits_drinking.isChecked
                newPatient.smoking = fragment_partner_details_habits_smoking.isChecked
                newPatient.tobaccochewing = fragment_partner_details_habits_tobacco.isChecked
                newPatient.partnerviolence = fragment_partner_details_habits_violence_yes.getCheckedString()
            }
            7 -> {
                newPatient.majordisease = fragment_family_history_disease_box.text.toString()
                val service = ApiClient.client.create(ApiService::class.java)
                val callRequest = service.createPatient(newPatient, accessTokenPref)
                dialog = indeterminateProgressDialog("Creating user...", "", {
                    setCancelable(true)
                    setOnCancelListener {
                        callRequest.cancel()
                    }
                })
                callRequest.enqueue(this)
                return
            }
        }

        fragmentPos++
        nextFragment()

    }

    private fun getTitleAndColor(): Pair<Int, Int> = when (fragmentPos) {
        1 -> R.string.fragment_new_user_personal_details_appbar to R.color.colorIndigo
        2 -> R.string.fragment_new_user_gh_appbar to R.color.colorPink
        3 -> R.string.fragment_new_user_mh_appbar to R.color.colorRedLite
        4 -> R.string.fragment_new_user_pp_appbar to R.color.colorBlueLight
        5 -> R.string.fragment_new_user_ppxp_appbar to R.color.colorRedLite
        6 -> R.string.fragment_new_user_partner_appbar to R.color.colorTeal
        7 -> R.string.fragment_new_user_family_history_appbar to R.color.colorTeal
        else -> R.string.fragment_new_user_aadhar_appbar to R.color.colorBlueLight
    }

    override fun onBackPressed() {
        super.onBackPressed()
        fragmentPos--
        changeTheme()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> finish()
        }
        return super.onOptionsItemSelected(item)
    }

    class NewUserFragment : Fragment(), DatePickerDialog.OnDateSetListener {

        private val fragmentList = arrayOf(R.layout.fragment_new_user_aadhar, R.layout.fragment_personal_details, R.layout.fragment_general_health,
                R.layout.fragment_medical_history, R.layout.fragment_previous_pregnancy, R.layout.fragment_previous_pregnancy_xp,
                R.layout.fragment_partner_details, R.layout.fragment_family_history)

        companion object {
            private val EXTRA_LAYOUT = "EXTRA_LAYOUT";
            fun load(fragmentType: Int): NewUserFragment {
                val fragment = NewUserFragment()
                val args = Bundle()
                args.putInt(EXTRA_LAYOUT, fragmentType)
                fragment.arguments = args
                return fragment
            }
        }

        override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
            val type = arguments!!.getInt(EXTRA_LAYOUT)
            return inflater.inflate(fragmentList[type], container, false)
        }

        override fun onActivityCreated(savedInstanceState: Bundle?) {
            super.onActivityCreated(savedInstanceState)
            // Aadhar
            fragment_new_user_aadhar_photo_fab?.setOnClickListener {
                (activity!! as NewUserActivity).checkForPermission(Manifest.permission.CAMERA) { granted ->
                    (activity!! as NewUserActivity).performCameraAction(granted, RC_IMAGE_CAPTURE_AADHAR)
                }
            }

            fragment_new_user_photo_fab?.setOnClickListener {
                (activity!! as NewUserActivity).checkForPermission(Manifest.permission.CAMERA) { granted ->
                    (activity!! as NewUserActivity).performCameraAction(granted, RC_IMAGE_CAPTURE)
                }
            }
            // Personal details
            newuser_personalinfo_duedate_name?.setOnClickListener { showDatePicker() }
            newuser_personalinfo_duedate_name?.setOnFocusChangeListener { _, hasFocus ->
                if (hasFocus) showDatePicker()
            }
            newuser_personalinfo_colony_name?.setOnFocusChangeListener { _, hasFocus ->
                if (hasFocus) loadVillageNames()
            }
        }

        private fun loadVillageNames() {
            val dialog = indeterminateProgressDialog("Getting village names...", "", {
                setCancelable(true)
            })
            val service = ApiClient.client.create(ApiService::class.java)
            val villageRequest = service.loadVillageNames(activity!!.accessTokenPref)

            val callback = object : Callback<Array<Village>> {
                override fun onResponse(call: Call<Array<Village>>, response: Response<Array<Village>>?) {
                    dialog.dismiss()
                    if (response != null) {
                        val resp = response.body()
                        if (resp != null) {
                            val villages: Array<String> = Array<String>(resp.size) { pos ->
                                resp[pos].villagename!!
                            }
                            newuser_personalinfo_colony_name.setAdapter(ArrayAdapter<String>(activity, android.R.layout.simple_list_item_1, villages))
                        } else {
                            toast("Error getting village names!")
                        }
                    } else {
                        toast("Error getting village names!")
                    }
                }

                override fun onFailure(call: Call<Array<Village>>?, t: Throwable?) {
                    dialog.dismiss()
                    toast("Error getting village names!")
                }

            }

            villageRequest.enqueue(callback)
        }

        private fun showDatePicker() {
            val cal = Calendar.getInstance()
            cal.timeInMillis = System.currentTimeMillis()
            val datePicker = DatePickerDialog(activity, this, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH))
            datePicker.show()
            activity?.hideKeyboard(activity?.newuser_layout?.windowToken)
        }

        override fun onDateSet(p0: DatePicker?, year: Int, month: Int, day: Int) {
            val cal = Calendar.getInstance()
            cal.set(year, month, day)
            newuser_personalinfo_duedate_name.setText(cal formatAs resources.getString(R.string.format_date_dd_MM_yy))
            newuser_personalinfo_phone_number.requestFocus()
        }

    }

}