package com.jiovio.olivewear.rural.savemom.util

import android.Manifest
import android.bluetooth.BluetoothAdapter
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import com.jiovio.olivewear.rural.savemom.data.OliveWear

/**
 * Created by abara on 01/09/17.
 */
object BluetoothUtils {
    private val bluetoothAdapter by lazy(LazyThreadSafetyMode.NONE) { BluetoothAdapter.getDefaultAdapter() }
    private val RC_ENABLE_BT = 101
    private val RC_ENABLE_PERMISSION = 102

    fun enabled(context: Fragment) =
            if (!bluetoothAdapter.isEnabled) {
                val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
                context.startActivityForResult(enableBtIntent, RC_ENABLE_BT)
                false
            } else {
                true
            }

    fun getPairedDevices(): List<OliveWear> {
        val pairedDevices = bluetoothAdapter.bondedDevices
        return pairedDevices.map { OliveWear(it) }
    }

    fun startDiscovery() = bluetoothAdapter.startDiscovery()

    fun cancelDiscovery() = bluetoothAdapter.startDiscovery()

    fun checkPermission(activity: FragmentActivity) =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                val permission = Manifest.permission.ACCESS_COARSE_LOCATION
                if (ActivityCompat.checkSelfPermission(activity, permission) ==
                        PackageManager.PERMISSION_GRANTED) {
                    true
                } else {
                    activity.requestPermissions(arrayOf(permission), RC_ENABLE_PERMISSION)
                    false
                }
            } else {
                true
            }

}