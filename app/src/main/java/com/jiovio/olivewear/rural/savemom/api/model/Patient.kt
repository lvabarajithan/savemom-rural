package com.jiovio.olivewear.rural.savemom.api.model

import android.os.Parcel
import android.os.Parcelable

/**
 * Created by abara on 11/09/17.
 */
data class Patient(
        var _id: String? = null, //59db6b413e07160004a11d25
        var image: String? = null, // img
        var aadharimg: String? = null, // base 64
        var aadharno: String? = null, //123456789012
        var name: String? = null, //Harini
        var fathername: String? = null, //Raj
        var husbandname: String? = null, //Kamal
        var villagename: String? = null, //Vellarankunnu
        var district: String? = null, //Madurai
        var state: String? = null, //Tamilnadu
        var pincode: String? = null, //625001
        var age: String? = null, //28
        var duedate: String? = null, //13-12-2017
        var phoneno: String? = null, //9976896745
        var firstpregnancy: String? = null, //no
        var howmanymonths: String? = null, //3
        var howmanychildren: String? = null, //1
        var ageofchildern: String? = null, //2
        var previousantenatal: String? = null, //yes
        var howmanyantenatal: String? = null, //1
        var takemedicineandvitamins: String? = null, //yes
        var abortion: String? = null, //no
        var stillbirth: String? = null, //no
        var undernourished: Boolean = false, //true
        var lowvitamin: Boolean = false, //true
        var lowprotien: Boolean = false, //true
        var lowcalcium: Boolean = false, //true
        var work: String? = null, //notworking
        var bp: String? = null, //yes
        var diabetics: String? = null, //no
        var anemia: String? = null, //no
        var tobacco: String? = null, //no
        var cancer: String? = null, //no
        var hiv: String? = null, //no
        var tb: String? = null, //no
        var husbandworking: String? = null, //yes
        var drinking: Boolean = false, //true
        var smoking: Boolean = false, //true
        var tobaccochewing: Boolean = false, //true
        var partnerviolence: String? = null, //no
        var majordisease: String? = null, //test
        var babyborn: ArrayList<Babyborn>? = null,
        var tests: Tests? = null,
        var checkup: ArrayList<Checkup>? = null,
        var lastdate: String? = null, //12-10-2017
        var color: String? = null //yellow
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readByte() != 0.toByte(),
            parcel.readByte() != 0.toByte(),
            parcel.readByte() != 0.toByte(),
            parcel.readByte() != 0.toByte(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readByte() != 0.toByte(),
            parcel.readByte() != 0.toByte(),
            parcel.readByte() != 0.toByte(),
            parcel.readString(),
            parcel.readString(),
            parcel.createTypedArrayList(Babyborn),
            parcel.readParcelable(Tests::class.java.classLoader),
            parcel.createTypedArrayList(Checkup),
            parcel.readString(),
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(_id)
        parcel.writeString(image)
        parcel.writeString(aadharimg)
        parcel.writeString(aadharno)
        parcel.writeString(name)
        parcel.writeString(fathername)
        parcel.writeString(husbandname)
        parcel.writeString(villagename)
        parcel.writeString(district)
        parcel.writeString(state)
        parcel.writeString(pincode)
        parcel.writeString(age)
        parcel.writeString(duedate)
        parcel.writeString(phoneno)
        parcel.writeString(firstpregnancy)
        parcel.writeString(howmanymonths)
        parcel.writeString(howmanychildren)
        parcel.writeString(ageofchildern)
        parcel.writeString(previousantenatal)
        parcel.writeString(howmanyantenatal)
        parcel.writeString(takemedicineandvitamins)
        parcel.writeString(abortion)
        parcel.writeString(stillbirth)
        parcel.writeByte(if (undernourished) 1 else 0)
        parcel.writeByte(if (lowvitamin) 1 else 0)
        parcel.writeByte(if (lowprotien) 1 else 0)
        parcel.writeByte(if (lowcalcium) 1 else 0)
        parcel.writeString(work)
        parcel.writeString(bp)
        parcel.writeString(diabetics)
        parcel.writeString(anemia)
        parcel.writeString(tobacco)
        parcel.writeString(cancer)
        parcel.writeString(hiv)
        parcel.writeString(tb)
        parcel.writeString(husbandworking)
        parcel.writeByte(if (drinking) 1 else 0)
        parcel.writeByte(if (smoking) 1 else 0)
        parcel.writeByte(if (tobaccochewing) 1 else 0)
        parcel.writeString(partnerviolence)
        parcel.writeString(majordisease)
        parcel.writeTypedList(babyborn)
        parcel.writeParcelable(tests, flags)
        parcel.writeTypedList(checkup)
        parcel.writeString(lastdate)
        parcel.writeString(color)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Patient> {
        override fun createFromParcel(parcel: Parcel): Patient {
            return Patient(parcel)
        }

        override fun newArray(size: Int): Array<Patient?> {
            return arrayOfNulls(size)
        }
    }

}

data class Tests(
        var bhcg: Bhcg? = null
) : Parcelable {
    constructor(parcel: Parcel) : this(parcel.readParcelable<Bhcg>(Bhcg::class.java.classLoader)) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeParcelable(bhcg, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Tests> {
        override fun createFromParcel(parcel: Parcel): Tests {
            return Tests(parcel)
        }

        override fun newArray(size: Int): Array<Tests?> {
            return arrayOfNulls(size)
        }
    }
}

data class Bhcg(
        var done: Boolean = false,
        var status: String? = null
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readByte() != 0.toByte(),
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeByte(if (done) 1 else 0)
        parcel.writeString(status)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Bhcg> {
        override fun createFromParcel(parcel: Parcel): Bhcg {
            return Bhcg(parcel)
        }

        override fun newArray(size: Int): Array<Bhcg?> {
            return arrayOfNulls(size)
        }
    }

}

data class Babyborn(
        var babyborn: String? = null, //yes
        var complications: String? = null, //no
        var motherisfine: String? = null, //yes
        var childisfine: String? = null, //yes
        var noofbaby: String? = null, //1
        var gender: String? = null, //Female
        var weightofchild: String? = null, //2
        var date: String? = null, //12-10-2017
        var username: String? = null //uma
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(babyborn)
        parcel.writeString(complications)
        parcel.writeString(motherisfine)
        parcel.writeString(childisfine)
        parcel.writeString(noofbaby)
        parcel.writeString(gender)
        parcel.writeString(weightofchild)
        parcel.writeString(date)
        parcel.writeString(username)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Babyborn> {
        override fun createFromParcel(parcel: Parcel): Babyborn {
            return Babyborn(parcel)
        }

        override fun newArray(size: Int): Array<Babyborn?> {
            return arrayOfNulls(size)
        }
    }

}

data class Checkup(
        var date: String? = null, //11-10-2017
        var reports: ArrayList<Report>? = null,
        var vitals: Vitals? = null,
        var done: Boolean = false, //false
        var color: String? = null, //red
        var userid: String? = null, //NPyHrA
        var nutrition: Nutrition? = null,
        var symptoms: Symptoms? = null,
        var supply: Supply? = null,
        var reminder: String? = null, //yes
        var prescription: ArrayList<Prescription>? = null,
        var comments: ArrayList<Comment>? = null
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.createTypedArrayList(Report),
            parcel.readParcelable(Vitals::class.java.classLoader),
            parcel.readByte() != 0.toByte(),
            parcel.readString(),
            parcel.readString(),
            parcel.readParcelable(Nutrition::class.java.classLoader),
            parcel.readParcelable(Symptoms::class.java.classLoader),
            parcel.readParcelable(Supply::class.java.classLoader),
            parcel.readString(),
            parcel.createTypedArrayList(Prescription),
            parcel.createTypedArrayList(Comment)) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(date)
        parcel.writeTypedList(reports)
        parcel.writeParcelable(vitals, flags)
        parcel.writeByte(if (done) 1 else 0)
        parcel.writeString(color)
        parcel.writeString(userid)
        parcel.writeParcelable(nutrition, flags)
        parcel.writeParcelable(symptoms, flags)
        parcel.writeParcelable(supply, flags)
        parcel.writeString(reminder)
        parcel.writeTypedList(prescription)
        parcel.writeTypedList(comments)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Checkup> {
        override fun createFromParcel(parcel: Parcel): Checkup {
            return Checkup(parcel)
        }

        override fun newArray(size: Int): Array<Checkup?> {
            return arrayOfNulls(size)
        }
    }

}

data class Report(var reportimg: String? = null) : Parcelable {
    constructor(parcel: Parcel) : this(parcel.readString())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(reportimg)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Report> {
        override fun createFromParcel(parcel: Parcel): Report {
            return Report(parcel)
        }

        override fun newArray(size: Int): Array<Report?> {
            return arrayOfNulls(size)
        }
    }
}

data class Prescription(
        var doctorname: String? = null, //Dr. Uma
        var username: String? = null, //uma
        var mobile: Long = 0, //9048751123
        var date: String? = null, //11-10-2017
        var prescriptiondate: String? = null, //12-10-2017
        var prescriptiontime: String? = null, //13:11
        var role: String? = null, //online
        var data: ArrayList<Data>? = null
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readLong(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.createTypedArrayList(Data)) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(doctorname)
        parcel.writeString(username)
        parcel.writeLong(mobile)
        parcel.writeString(date)
        parcel.writeString(prescriptiondate)
        parcel.writeString(prescriptiontime)
        parcel.writeString(role)
        parcel.writeTypedList(data)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Prescription> {
        override fun createFromParcel(parcel: Parcel): Prescription {
            return Prescription(parcel)
        }

        override fun newArray(size: Int): Array<Prescription?> {
            return arrayOfNulls(size)
        }
    }


}

data class Data(
        var tabletname: String? = null, //Vitamin Tablets
        var dosage: Int = 0, //10
        var morning: Int = 0, //1
        var morningfood: String? = null, //After Food
        var night: Int = 0, //1
        var nightfood: String? = null //After Food
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readString(),
            parcel.readInt(),
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(tabletname)
        parcel.writeInt(dosage)
        parcel.writeInt(morning)
        parcel.writeString(morningfood)
        parcel.writeInt(night)
        parcel.writeString(nightfood)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Data> {
        override fun createFromParcel(parcel: Parcel): Data {
            return Data(parcel)
        }

        override fun newArray(size: Int): Array<Data?> {
            return arrayOfNulls(size)
        }
    }

}

data class Nutrition(
        var regularantenatalvisit: String? = null, //yes
        var stressofwork: String? = null, //yes
        var takingmedicines: String? = null, //yes
        var takingwater: String? = null, //yes
        var eatingprotienfood: String? = null, //yes
        var happywithfamily: String? = null, //yes
        var comsumingtobbacco: String? = null, //no
        var consumingcoffee: String? = null //no
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(regularantenatalvisit)
        parcel.writeString(stressofwork)
        parcel.writeString(takingmedicines)
        parcel.writeString(takingwater)
        parcel.writeString(eatingprotienfood)
        parcel.writeString(happywithfamily)
        parcel.writeString(comsumingtobbacco)
        parcel.writeString(consumingcoffee)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Nutrition> {
        override fun createFromParcel(parcel: Parcel): Nutrition {
            return Nutrition(parcel)
        }

        override fun newArray(size: Int): Array<Nutrition?> {
            return arrayOfNulls(size)
        }
    }

}

data class Symptoms(
        var blurredvision: String? = null, //yes
        var dizziness: String? = null, //yes
        var shortnessofbreath: String? = null, //yes
        var legedema: String? = null, //yes
        var chestpain: String? = null, //yes
        var abdominalpain: String? = null, //yes
        var vomiting: String? = null, //yes
        var stomachburning: String? = null, //yes
        var urinaryburning: String? = null, //yes
        var bleeding: String? = null, //yes
        var feverandchills: String? = null, //yes
        var rashesonskin: String? = null, //yes
        var vaginalitching: String? = null, //yes
        var asthma: String? = null, //yes
        var bodypain: String? = null, //yes
        var cough: String? = null, //yes
        var sourtaste: String? = null, //yes
        var headache: String? = null //yes
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(blurredvision)
        parcel.writeString(dizziness)
        parcel.writeString(shortnessofbreath)
        parcel.writeString(legedema)
        parcel.writeString(chestpain)
        parcel.writeString(abdominalpain)
        parcel.writeString(vomiting)
        parcel.writeString(stomachburning)
        parcel.writeString(urinaryburning)
        parcel.writeString(bleeding)
        parcel.writeString(feverandchills)
        parcel.writeString(rashesonskin)
        parcel.writeString(vaginalitching)
        parcel.writeString(asthma)
        parcel.writeString(bodypain)
        parcel.writeString(cough)
        parcel.writeString(sourtaste)
        parcel.writeString(headache)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Symptoms> {
        override fun createFromParcel(parcel: Parcel): Symptoms {
            return Symptoms(parcel)
        }

        override fun newArray(size: Int): Array<Symptoms?> {
            return arrayOfNulls(size)
        }
    }

}

data class Comment(
        var comment: String? = null, //Need immediate care.
        var emergency: Boolean = false, //true
        var sendtooncalldoctor: Boolean = false, //true
        var doctorname: String? = null, //Dr. Uma
        var username: String? = null, //uma
        var mobile: Long = 0, //9048751123
        var date: String? = null, //11-10-2017
        var commentdate: String? = null, //12-10-2017
        var commenttime: String? = null, //13:11
        var role: String? = null //online
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readByte() != 0.toByte(),
            parcel.readByte() != 0.toByte(),
            parcel.readString(),
            parcel.readString(),
            parcel.readLong(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(comment)
        parcel.writeByte(if (emergency) 1 else 0)
        parcel.writeByte(if (sendtooncalldoctor) 1 else 0)
        parcel.writeString(doctorname)
        parcel.writeString(username)
        parcel.writeLong(mobile)
        parcel.writeString(date)
        parcel.writeString(commentdate)
        parcel.writeString(commenttime)
        parcel.writeString(role)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Comment> {
        override fun createFromParcel(parcel: Parcel): Comment {
            return Comment(parcel)
        }

        override fun newArray(size: Int): Array<Comment?> {
            return arrayOfNulls(size)
        }
    }

}

data class Supply(
        var nutritionsuppliments: String? = null, //yes
        var pholicacidtablet: String? = null, //yes
        var informquantity: String? = null //yes
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(nutritionsuppliments)
        parcel.writeString(pholicacidtablet)
        parcel.writeString(informquantity)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Supply> {
        override fun createFromParcel(parcel: Parcel): Supply {
            return Supply(parcel)
        }

        override fun newArray(size: Int): Array<Supply?> {
            return arrayOfNulls(size)
        }
    }

}

data class Vitals(
        var bp: Bp? = null,
        var spo2: Spo2? = null,
        var height: String? = null,
        var weight: String? = null,
        var bmi: String? = null,
        var gainedweight: String? = null
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readParcelable(Bp::class.java.classLoader),
            parcel.readParcelable(Spo2::class.java.classLoader),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeParcelable(bp, flags)
        parcel.writeParcelable(spo2, flags)
        parcel.writeString(height)
        parcel.writeString(weight)
        parcel.writeString(bmi)
        parcel.writeString(gainedweight)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Vitals> {
        override fun createFromParcel(parcel: Parcel): Vitals {
            return Vitals(parcel)
        }

        override fun newArray(size: Int): Array<Vitals?> {
            return arrayOfNulls(size)
        }
    }

}

data class Spo2(var value: String? = null) : Parcelable {

    constructor(parcel: Parcel) : this(parcel.readString())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(value)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Spo2> {
        override fun createFromParcel(parcel: Parcel): Spo2 {
            return Spo2(parcel)
        }

        override fun newArray(size: Int): Array<Spo2?> {
            return arrayOfNulls(size)
        }
    }

}

data class Bp(
        var systolic: String? = null, //123
        var diastolic: String? = null, //456
        var pulse: String? = null, //86
        var bpcolor: String? = null, //#fdd047
        var bpvalue: String? = null //High BP
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(systolic)
        parcel.writeString(diastolic)
        parcel.writeString(pulse)
        parcel.writeString(bpcolor)
        parcel.writeString(bpvalue)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Bp> {
        override fun createFromParcel(parcel: Parcel): Bp {
            return Bp(parcel)
        }

        override fun newArray(size: Int): Array<Bp?> {
            return arrayOfNulls(size)
        }
    }
}

data class NewPatient(var _id: String? = null,
                      var success: Boolean = false,
                      var message: String? = null
) : Parcelable {
    override fun writeToParcel(p0: Parcel, p1: Int) {
        p0.writeString(_id)
        p0.writeValue(success)
        p0.writeString(message)
    }

    constructor(parcel: Parcel) : this() {
        _id = parcel.readString()
        success = parcel.readValue(Boolean::class.java.classLoader) as Boolean
        message = parcel.readString()
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<NewPatient> {
        override fun createFromParcel(parcel: Parcel): NewPatient {
            return NewPatient(parcel)
        }

        override fun newArray(size: Int): Array<NewPatient?> {
            return arrayOfNulls(size)
        }
    }

}

data class Reminder(var value: String? = null) : Parcelable {
    constructor(parcel: Parcel) : this(parcel.readString())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(value)
    }

    override fun describeContents(): Int = 0

    companion object CREATOR : Parcelable.Creator<Reminder> {
        override fun createFromParcel(parcel: Parcel): Reminder {
            return Reminder(parcel)
        }

        override fun newArray(size: Int): Array<Reminder?> {
            return arrayOfNulls(size)
        }
    }
}