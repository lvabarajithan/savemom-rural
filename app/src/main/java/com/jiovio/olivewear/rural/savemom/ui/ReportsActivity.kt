package com.jiovio.olivewear.rural.savemom.ui

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.os.Bundle
import android.provider.MediaStore
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.GridLayoutManager
import com.jiovio.olivehealth.rural.savemom.R
import com.jiovio.olivewear.rural.savemom.adapter.ReportsAdapter
import com.jiovio.olivewear.rural.savemom.api.model.Report
import com.jiovio.olivewear.rural.savemom.fragment.PatientsCheckupFragment
import com.jiovio.olivewear.rural.savemom.util.PERMISSION_REQUEST_CAMERA
import com.jiovio.olivewear.rural.savemom.util.asBase64String
import com.jiovio.olivewear.rural.savemom.util.checkForPermission
import kotlinx.android.synthetic.main.activity_reports.*
import org.jetbrains.anko.appcompat.v7.titleResource
import org.jetbrains.anko.toast

/**
 * Created by abara on 04/11/17.
 */
class ReportsActivity : AppCompatActivity() {

    companion object {
        const val EXTRA_ENABLE_ADD = "enable_add"
        const val EXTRA_REPORTS = "reports"
    }

    private var adapter: ReportsAdapter? = null
    private val RC_IMAGE_CAPTURE = 99

    private lateinit var reports: ArrayList<Report>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reports)
        reports_appbar.titleResource = R.string.report_title_appbar
        setSupportActionBar(reports_appbar)

        report_finish_list.layoutManager = GridLayoutManager(this, 3)
        report_finish_list.itemAnimator = DefaultItemAnimator()

        reports = intent.getParcelableArrayListExtra(EXTRA_REPORTS)

        val shouldEnableAdd = intent.getBooleanExtra(EXTRA_ENABLE_ADD, true)

        adapter = ReportsAdapter(reports, addAction, shouldEnableAdd)
        report_finish_list.adapter = adapter

        report_finish_button.setOnClickListener {

            if (shouldEnableAdd) {
                val data = Intent()
                data.putParcelableArrayListExtra(PatientsCheckupFragment.EXTRA_RESULT, reports)
                setResult(Activity.RESULT_OK, data)
            }
            finish()

        }

    }

    private val addAction = {
        checkForPermission(Manifest.permission.CAMERA) { granted ->
            performCameraAction(granted)
        }
    }

    private fun performCameraAction(granted: Boolean) {
        if (granted) {

            val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            if (intent.resolveActivity(packageManager) != null) {
                startActivityForResult(intent, RC_IMAGE_CAPTURE)
            }

        } else {
            toast("Permission required to access camera")
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if ((requestCode == RC_IMAGE_CAPTURE) and (resultCode == Activity.RESULT_OK)) {

            data?.let {
                val extras = data.extras
                val bitmapImage = extras.get("data") as Bitmap
                reports.add(Report(bitmapImage.asBase64String()))
                adapter = ReportsAdapter(reports, addAction)
                report_finish_list.adapter = adapter
            }

        }

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        performCameraAction(requestCode == PERMISSION_REQUEST_CAMERA
                && grantResults[0] == PackageManager.PERMISSION_GRANTED)
    }

}