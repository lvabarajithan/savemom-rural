package com.jiovio.olivewear.rural.savemom.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jiovio.olivehealth.rural.savemom.R
import kotlinx.android.synthetic.main.fragment_today.*

/**
 * Created by abara on 16/08/17.
 */
class TodayFragment : BaseFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val view = inflater.inflate(R.layout.fragment_today, container, false)
        toolbarColor(R.color.colorIndigo, R.color.colorWhite)
        setTitle(R.string.today_fragment_title)
        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        today_group_checkup.setOnClickListener {
            nextFragment(PatientListFragment.with(PatientListFragment.TYPE_GROUP_CHECKUP))
        }

        today_feedback.setOnClickListener {
            nextFragment(PatientListFragment.with(PatientListFragment.TYPE_FEEDBACK))
        }

    }
}
