package com.jiovio.olivewear.rural.savemom.fragment

import android.app.Activity
import android.bluetooth.BluetoothDevice
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jiovio.olivehealth.rural.savemom.R
import com.jiovio.olivewear.rural.savemom.adapter.DeviceListAdapter
import com.jiovio.olivewear.rural.savemom.data.OliveWear
import com.jiovio.olivewear.rural.savemom.receiver.BluetoothReceiver
import com.jiovio.olivewear.rural.savemom.util.BluetoothUtils
import kotlinx.android.synthetic.main.fragment_device_list.view.*

/**
 * Created by abara on 02/09/17.
 */
class DeviceListFragment : BaseFragment() {
    interface OnAddDevice {
        fun onAddDevice(device: OliveWear)
    }

    companion object {
        private val EXTRA_DEVICE = "extra_device"
        fun newInstance(): DeviceListFragment = DeviceListFragment()
    }

    private val mBluetoothReceiver by lazy(LazyThreadSafetyMode.NONE) {
        BluetoothReceiver {
            onDeviceFound(it)
        }
    }

    private val mDeviceListAdapter by lazy(LazyThreadSafetyMode.NONE) {
        DeviceListAdapter(ArrayList(BluetoothUtils.getPairedDevices())) {
            if (it.bluetoothDevice.bondState == BluetoothDevice.BOND_BONDED) {
                mListener?.onAddDevice(it)
            } else {
                onPairDevice(it.bluetoothDevice)
            }
        }
    }

    private var mListener: OnAddDevice? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_device_list, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        if (!BluetoothUtils.checkPermission(activity!!)) {
            if (BluetoothUtils.enabled(this)) {
                BluetoothUtils.startDiscovery()
            }
        } else {
            if (BluetoothUtils.enabled(this)) {
                BluetoothUtils.startDiscovery()
            }
        }

        view?.apply {
            initDeviceRecycler(device_list_recycler)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

    }

    private fun initDeviceRecycler(recyclerView: RecyclerView?) {
        recyclerView?.let {
            it.layoutManager = LinearLayoutManager(activity)
            it.adapter = mDeviceListAdapter
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            mDeviceListAdapter.addDevices(BluetoothUtils.getPairedDevices())
            BluetoothUtils.startDiscovery()
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        val filter = IntentFilter(BluetoothDevice.ACTION_FOUND)
        filter.addAction(BluetoothDevice.ACTION_PAIRING_REQUEST)
        filter.addAction(BluetoothDevice.ACTION_BOND_STATE_CHANGED)
        activity?.registerReceiver(mBluetoothReceiver, filter)
        if (context is OnAddDevice) {
            mListener = context
        }
    }

    private fun onDeviceFound(device: OliveWear) {
        mDeviceListAdapter.addDevice(device)
        nextFragment(AddDevicesFragment.newInstance(device))
    }

    private fun onPairDevice(device: BluetoothDevice) {
        device.setPin("0000".toByteArray())
        device.createBond()
    }

    override fun onDetach() {
        super.onDetach()
        activity?.unregisterReceiver(mBluetoothReceiver)
    }

}