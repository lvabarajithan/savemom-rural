package com.jiovio.olivewear.rural.savemom.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jiovio.olivehealth.rural.savemom.R
import com.jiovio.olivewear.rural.savemom.data.OliveWear
import com.jiovio.olivewear.rural.savemom.util.ProfileUtils
import kotlinx.android.synthetic.main.fragment_add_device.view.*

/**
 * Created by abara on 02/09/17.
 */
class AddDevicesFragment : BaseFragment() {

    interface OnSave {
        fun onSave()
    }

    companion object {
        private val EXTRA_DEVICE = "extra_device"
        fun newInstance(device: OliveWear): AddDevicesFragment {
            val addDeviceFragment = AddDevicesFragment()
            val args = Bundle()
            args.putParcelable(EXTRA_DEVICE, device)
            addDeviceFragment.arguments = args
            return addDeviceFragment
        }
    }

    private var mListener: OnSave? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_add_device, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val device = arguments?.getParcelable<OliveWear>(EXTRA_DEVICE)

        view.apply {
            add_device_save_btn.setOnClickListener {
                val selectedDeviceOrdinal = add_device_radio_group.indexOfChild(add_device_radio_group.findViewById(add_device_radio_group.checkedRadioButtonId))
                selectedDeviceOrdinal.let {
                    ProfileUtils.newInstance(activity)
                            .setDevice(selectedDeviceOrdinal, device?.bluetoothDevice?.address)
                    mListener?.onSave()
                }
            }
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnSave) {
            mListener = context
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }


}