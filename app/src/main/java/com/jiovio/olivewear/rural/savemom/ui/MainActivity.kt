package com.jiovio.olivewear.rural.savemom.ui

import android.content.res.Configuration
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.widget.AppCompatTextView
import android.view.MenuItem
import android.view.View
import com.jiovio.olivehealth.rural.savemom.R
import com.jiovio.olivewear.rural.savemom.fragment.BaseFragment
import com.jiovio.olivewear.rural.savemom.fragment.DeviceListFragment
import com.jiovio.olivewear.rural.savemom.fragment.TodayFragment
import com.jiovio.olivewear.rural.savemom.util.commitTransaction
import com.jiovio.olivewear.rural.savemom.util.parseColor
import com.jiovio.olivewear.rural.savemom.util.workerPref
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.find
import org.jetbrains.anko.textColor

class MainActivity : BaseActivity(), NavigationView.OnNavigationItemSelectedListener {

    private lateinit var drawerToggle: ActionBarDrawerToggle

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        main_appbar.title = ""
        setSupportActionBar(main_appbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        loadFragment(TodayFragment(), false)

        initViews()

    }

    private fun initViews() {

        main_navigation_view.setNavigationItemSelectedListener(this)

        drawerToggle = object : ActionBarDrawerToggle(this, main_drawer_layout, R.string.open_navigation_drawer, R.string.close_navigation_drawer) {

            override fun onDrawerOpened(drawerView: View) {
                super.onDrawerOpened(drawerView)
                invalidateOptionsMenu()
            }

            override fun onDrawerClosed(drawerView: View) {
                super.onDrawerClosed(drawerView)
                invalidateOptionsMenu()
            }

        }

        main_drawer_layout.post {
            drawerToggle.syncState()
        }

        drawerToggle.isDrawerIndicatorEnabled = true
        main_drawer_layout.addDrawerListener(drawerToggle)

        val worker = workerPref.data
        main_navigation_view.getHeaderView(0).find<AppCompatTextView>(R.id.drawer_header_workername).text = worker?.healthworkername ?: "Unknown"
        main_navigation_view.getHeaderView(0).find<AppCompatTextView>(R.id.drawer_header_workerid).text = "ID: ${worker?.userid ?: "Unknown Id"}"

    }

    override fun onConfigurationChanged(newConfig: Configuration?) {
        super.onConfigurationChanged(newConfig)
        drawerToggle.onConfigurationChanged(newConfig)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean =
            when (item.itemId) {
                R.id.action_devices -> {
                    loadFragment(DeviceListFragment.newInstance())
                    main_drawer_layout.closeDrawer(main_navigation_view)
                    true
                }
                R.id.action_logout -> {

                    true
                }
                else -> false
            }


    override fun changeToolbarColor(color: Int, titleColor: Int) {
        supportActionBar?.setBackgroundDrawable(ColorDrawable(parseColor(color)))
        main_appbar_title.textColor = parseColor(titleColor)
    }

    override fun loadFragment(fragment: BaseFragment?, addToBackStack: Boolean) {
        fragment?.let {
            supportFragmentManager.commitTransaction {
                if (addToBackStack) addToBackStack(null)
                replace(R.id.main_content, fragment)
            }
        }
    }

    override fun onBackPressed() {
        if (main_drawer_layout.isDrawerOpen(main_navigation_view)) {
            main_drawer_layout.closeDrawer(main_navigation_view)
        } else {
            super.onBackPressed()
        }
    }

}
