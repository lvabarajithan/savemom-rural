package com.jiovio.olivewear.rural.savemom.adapter

import android.support.constraint.ConstraintLayout
import android.support.v7.widget.AppCompatImageView
import android.support.v7.widget.AppCompatTextView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jiovio.olivehealth.rural.savemom.R
import com.jiovio.olivewear.rural.savemom.api.model.Checkup
import org.jetbrains.anko.find
import org.jetbrains.anko.imageResource

/**
 * Created by abara on 15/11/17.
 */
class CheckupAdapter(private val checkups: ArrayList<Checkup>, private val clickAction: (lastDate: String, checkup: Checkup) -> Unit) : RecyclerView.Adapter<CheckupAdapter.CheckupHolder>() {

    private val CHECKUP_YELLOW_COLOR = "yellow"
    private val CHECKUP_RED_COLOR = "red"
    private val CHECKUP_GREEN_COLOR = "green"

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): CheckupHolder {
        val view = LayoutInflater.from(parent?.context).inflate(R.layout.single_item_checkup_list, parent, false)
        return CheckupHolder(view)
    }

    override fun onBindViewHolder(holder: CheckupHolder, pos: Int) {

        val checkup = checkups[pos]
        holder.title.text = "Test on ${checkup.date}"
        holder.desc.text = "Last update on ${checkups.last().date}"
        holder.color.imageResource = when (checkup.color?.toLowerCase()) {
            CHECKUP_GREEN_COLOR -> R.drawable.ic_round_teal
            CHECKUP_RED_COLOR -> R.drawable.ic_round_red
            CHECKUP_YELLOW_COLOR -> R.drawable.ic_round_yellow
            else -> 0
        }
        holder.layout.setOnClickListener { clickAction(holder.desc.text.toString(), checkup) }

    }

    override fun getItemCount(): Int = checkups.size

    class CheckupHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val title = view.find<AppCompatTextView>(R.id.item_checkup_review_title)
        val desc = view.find<AppCompatTextView>(R.id.item_checkup_review_desc)
        val color = view.find<AppCompatImageView>(R.id.item_checkup_review_color)
        val layout = view.find<ConstraintLayout>(R.id.item_checkup_review_layout)
    }

}