package com.jiovio.olivewear.rural.savemom.util

import android.content.Context
import android.content.SharedPreferences

object PreferenceUtils {

    private val PREFERENCE_FILE_NAME = "common_preferences"

    private lateinit var mSharedPreference: SharedPreferences

    fun newInstance(context: Context?): PreferenceUtils {
        mSharedPreference = context!!.getSharedPreferences(PREFERENCE_FILE_NAME, Context.MODE_PRIVATE)
        return this
    }

    fun newInstance(context: Context, preferenceFileName: String): PreferenceUtils {
        mSharedPreference = context.getSharedPreferences(preferenceFileName, Context.MODE_PRIVATE)
        return this
    }

    fun getBooleanPrefs(key: String, defaultValue: Boolean = false) =
            mSharedPreference.getBoolean(key, defaultValue)

    fun getStringPrefs(key: String, defaultValue: String = "") =
            mSharedPreference.getString(key, defaultValue)

    operator fun set(key: String, value: Any) {
        val sharedPreferenceEditor = mSharedPreference.edit()
        if (value is String) {
            sharedPreferenceEditor.putString(key, value)
        } else if (value is Long) {
            sharedPreferenceEditor.putLong(key, value)
        } else if (value is Int) {
            sharedPreferenceEditor.putInt(key, value)
        } else if (value is Boolean) {
            sharedPreferenceEditor.putBoolean(key, value)
        } else if (value is Float) {
            sharedPreferenceEditor.putFloat(key, value)
        }
        sharedPreferenceEditor.apply()
    }

    fun setStringSet(key: String, value: Set<String>) {
        val sharedPreferenceEditor = mSharedPreference.edit()
        sharedPreferenceEditor.putStringSet(key, value)
        sharedPreferenceEditor.apply()
    }

    fun getStringSet(key: String, defaultValue: Set<String> = emptySet()): Set<String> =
            mSharedPreference.getStringSet(key, defaultValue)

    fun clear() {
        mSharedPreference.edit().clear().apply()
    }
}
