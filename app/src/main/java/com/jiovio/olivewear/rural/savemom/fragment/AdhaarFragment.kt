package com.jiovio.olivewear.rural.savemom.fragment

import android.app.ProgressDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jiovio.olivehealth.rural.savemom.R
import com.jiovio.olivewear.rural.savemom.api.ApiClient
import com.jiovio.olivewear.rural.savemom.api.ApiService
import com.jiovio.olivewear.rural.savemom.api.model.Patient
import com.jiovio.olivewear.rural.savemom.ui.MeasureActivity
import com.jiovio.olivewear.rural.savemom.util.accessTokenPref
import kotlinx.android.synthetic.main.fragment_adhaar.*
import org.jetbrains.anko.support.v4.indeterminateProgressDialog
import org.jetbrains.anko.support.v4.startActivity
import org.jetbrains.anko.support.v4.toast
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by abara on 17/08/17.
 */
class AdhaarFragment : BaseFragment(), Callback<Patient> {

    private lateinit var dialog: ProgressDialog

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val view = inflater.inflate(R.layout.fragment_adhaar, container, false)
        setTitle(R.string.today_fragment_title)
        toolbarColor(R.color.colorAccent, R.color.colorWhite)
        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        fragment_adhaar_start.setOnClickListener {
            verifyAdhaar(fragment_adhaar_text.text.toString())
        }

    }

    private fun verifyAdhaar(adhaarNo: String) {

        if (!adhaarNo.isEmpty()) {
            dialog = indeterminateProgressDialog("Verifying...", "", { setCancelable(false) })
            val service = ApiClient.client.create(ApiService::class.java)
            service.getPatient(adhaarNo, activity!!.accessTokenPref).enqueue(this)
        } else {
            toast("Enter Adhaar number")
        }

    }

    override fun onResponse(call: Call<Patient>, response: Response<Patient>) {

        val patient = response.body()
        if (patient != null) {
            startActivity<MeasureActivity>("patient" to patient)
            toast("Measure for ${patient.name}")
        } else {
            toast("Incorrect Adhaar number")
        }

        dialog.dismiss()
    }

    override fun onFailure(call: Call<Patient>?, t: Throwable?) {
        toast("Verification failed!")
        dialog.dismiss()
    }

}