package com.jiovio.olivewear.rural.savemom.ui

import android.app.ProgressDialog
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.jiovio.olivehealth.rural.savemom.R
import com.jiovio.olivewear.rural.savemom.api.ApiClient
import com.jiovio.olivewear.rural.savemom.api.ApiService
import com.jiovio.olivewear.rural.savemom.api.model.Worker
import com.jiovio.olivewear.rural.savemom.api.model.WorkerBody
import com.jiovio.olivewear.rural.savemom.util.accessTokenPref
import com.jiovio.olivewear.rural.savemom.util.hideKeyboard
import com.jiovio.olivewear.rural.savemom.util.isLoggedIn
import com.jiovio.olivewear.rural.savemom.util.workerPref
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.login_layout.*
import org.jetbrains.anko.indeterminateProgressDialog
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.toast
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

/**
 * Created by abara on 16/08/17.
 */
class LoginActivity : AppCompatActivity(), Callback<Worker> {

    private lateinit var dialog: ProgressDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        displaySplashFor(2000L)

        login_button.setOnClickListener {

            val uniqueId = login_uniqueid_box.text.toString()
            if (!uniqueId.isEmpty()) {
                dialog = indeterminateProgressDialog("Logging in...", "", {
                    setCancelable(false)
                })
                val service = ApiClient.client.create(ApiService::class.java)
                service.loginWorker(WorkerBody(uniqueId)).enqueue(this)
            }

        }

    }

    private fun displaySplashFor(millis: Long) {

        Timer().schedule(object : TimerTask() {
            override fun run() {
                runOnUiThread {
                    loginLayout.visibility = View.VISIBLE
                    loginSplashLayout.visibility = View.GONE
                }
            }
        }, millis)

    }

    override fun onResponse(call: Call<Worker>?, response: Response<Worker>) {
        val worker = response.body()
        if (worker != null) {
            if (worker.success) {
                loginSuccess(worker)
            } else {
                toast("Unique Id not registered.")
            }
        } else {
            toast("Login failed!")
        }
        dialog.dismiss()
    }

    override fun onFailure(call: Call<Worker>?, t: Throwable?) {
        t?.printStackTrace()
        toast("Try again.")
        dialog.dismiss()
    }

    private fun loginSuccess(worker: Worker) {
        hideKeyboard(login_layout.windowToken)

        isLoggedIn = true
        workerPref = worker
        accessTokenPref = worker.token!!

        startActivity(intentFor<MainActivity>())
        finish()
    }

}