package com.jiovio.olivewear.rural.savemom.adapter

import android.content.res.TypedArray
import android.support.v7.widget.AppCompatImageView
import android.support.v7.widget.AppCompatTextView
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.jiovio.olivehealth.rural.savemom.R
import org.jetbrains.anko.backgroundColor
import org.jetbrains.anko.imageResource

/**
 * Created by abara on 26/08/17.
 */
class PatientsCheckupAdapter(private val clickAction: (pos: Int, title: String) -> Unit) : RecyclerView.Adapter<PatientsCheckupAdapter.FeedbackCheckupHolder>() {
    private var testTitles: Array<String> = emptyArray()
    private var colorsArray: IntArray = IntArray(0)
    private var imagesArray: TypedArray? = null
    private var colorSize = 0

    override fun onBindViewHolder(holder: FeedbackCheckupHolder, position: Int) {
        holder.title.text = testTitles[position]
        holder.layout.setOnClickListener {
            clickAction(holder.adapterPosition, testTitles[holder.adapterPosition])
        }
        holder.card.backgroundColor = colorsArray[holder.adapterPosition % colorSize]
        holder.img.imageResource = imagesArray!!.getResourceId(holder.adapterPosition, -1)
        imagesArray!!.recycle()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FeedbackCheckupHolder {
        val context = parent.context
        testTitles = context.resources.getStringArray(R.array.feedback_user_checkup)
        colorsArray = context.resources.getIntArray(R.array.feedback_colors)
        imagesArray = context.resources.obtainTypedArray(R.array.feedback_user_checkup_img)
        colorSize = colorsArray.size
        val view = LayoutInflater.from(context).inflate(R.layout.single_item_patient_checkup, parent, false)
        return FeedbackCheckupHolder(view)
    }

    override fun getItemCount(): Int = 6

    class FeedbackCheckupHolder(view: View) : RecyclerView.ViewHolder(view) {
        val layout = view.findViewById<LinearLayout>(R.id.item_checkup_layout)
        val title = view.findViewById<AppCompatTextView>(R.id.item_checkup_title)
        val img = view.findViewById<AppCompatImageView>(R.id.item_checkup_img)
        val card = view.findViewById<CardView>(R.id.item_checkup_card)
    }

}