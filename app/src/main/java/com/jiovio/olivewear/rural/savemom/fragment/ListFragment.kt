package com.jiovio.olivewear.rural.savemom.fragment

import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jiovio.olivehealth.rural.savemom.R
import kotlinx.android.synthetic.main.fragment_list.*

/**
 * Created by abara on 25/08/17.
 */
open class ListFragment : BaseFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_list, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        toolbarColor()

        fragment_list.layoutManager = LinearLayoutManager(activity)
        fragment_list.itemAnimator = DefaultItemAnimator()
        fragment_list.setHasFixedSize(false)
    }

}