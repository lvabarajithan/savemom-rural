package com.jiovio.olivewear.rural.savemom.fragment

import android.app.Activity.RESULT_OK
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.view.View
import com.jiovio.olivehealth.rural.savemom.R
import com.jiovio.olivewear.rural.savemom.adapter.PatientsCheckupAdapter
import com.jiovio.olivewear.rural.savemom.api.ApiClient
import com.jiovio.olivewear.rural.savemom.api.ApiService
import com.jiovio.olivewear.rural.savemom.api.model.*
import com.jiovio.olivewear.rural.savemom.ui.CheckupsActivity
import com.jiovio.olivewear.rural.savemom.ui.MeasureActivity
import com.jiovio.olivewear.rural.savemom.ui.ReportsActivity
import com.jiovio.olivewear.rural.savemom.util.accessTokenPref
import com.jiovio.olivewear.rural.savemom.util.formatNowAs
import com.jiovio.olivewear.rural.savemom.util.workerPref
import kotlinx.android.synthetic.main.fragment_list.*
import org.jetbrains.anko.noButton
import org.jetbrains.anko.support.v4.alert
import org.jetbrains.anko.support.v4.longToast
import org.jetbrains.anko.support.v4.startActivityForResult
import org.jetbrains.anko.support.v4.toast
import org.jetbrains.anko.yesButton
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.ArrayList

/**
 * Created by abara on 17/09/17.
 */
class PatientsCheckupFragment : ListFragment(), Callback<Patient> {

    companion object {
        private const val RC_MEASURE = 11
        private const val RC_REPORTS = 12
        private const val RC_CHECKUPS = 13
        const val EXTRA_RESULT = "extra_result"
        fun getInstance(title: String, patient: Patient?): PatientsCheckupFragment {
            val fragment = PatientsCheckupFragment()
            val args = Bundle()
            patient?.let {
                args.putParcelable("patient", patient)
            }
            args.putString("title", title)
            fragment.arguments = args
            return fragment
        }
    }

    private var checkup = Checkup()

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val toolbarTitle = arguments!!.getString("title")
        setTitle(toolbarTitle)

        checkup.date = Calendar.getInstance().formatNowAs("dd-MM-yyyy")
        checkup.color = "yellow"
        checkup.userid = activity!!.workerPref.data!!.userid
        checkup.done = false

        val patient = arguments!!.getParcelable<Patient>("patient")

        if (patient != null) {

            fragment_list_finish.visibility = View.VISIBLE
            fragment_list.layoutManager = GridLayoutManager(activity, 2)
            fragment_list.adapter = PatientsCheckupAdapter({ pos, title ->
                when (pos) {
                    0 -> {
                        val lastWeight: Int = patient.checkup?.last()?.vitals?.weight?.toFloat()?.toInt() ?: -1
                        startActivityForResult<MeasureActivity>(RC_MEASURE, "last_weight" to lastWeight)
                    }
                    4 -> startActivityForResult<ReportsActivity>(RC_REPORTS, "reports" to (checkup.reports ?: ArrayList()))
                    else -> startActivityForResult<CheckupsActivity>(RC_CHECKUPS, CheckupsActivity.EXTRA_TYPE to pos, CheckupsActivity.EXTRA_TITLE to title)
                }

            })

        } else {
            toast("This patient is invalid.")
        }

        fragment_list_finish.setOnClickListener {

            val service = ApiClient.client.create(ApiService::class.java)

            val dateString = Calendar.getInstance() formatNowAs resources.getString(R.string.format_date_dd_MM_yy)
            checkup.date = dateString

            val userId = activity!!.workerPref.data!!.userid!!
            val accessToken = activity!!.accessTokenPref

            alert("", "Checkup done?") {
                yesButton {
                    service.createCheckup(patient.aadharno!!, patient.villagename!!, dateString, userId, checkup, accessToken).enqueue(this@PatientsCheckupFragment)
                    longToast("Creating checkup\nPlease wait...")
                }
                noButton {}
            }.show()

        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == RESULT_OK) {
            when (requestCode) {
                RC_MEASURE -> {
                    val vitals = data?.getParcelableExtra<Vitals>(EXTRA_RESULT)
                    checkup.vitals = vitals
                }
                RC_CHECKUPS -> {
                    val type = data?.getIntExtra(CheckupsActivity.EXTRA_TYPE, 1) ?: 1
                    updateCheckupForType(type, data)
                }
                RC_REPORTS -> {
                    val reports = data?.getParcelableArrayListExtra<Report>(EXTRA_RESULT)
                    checkup.reports = reports
                    toast("${reports?.size} Report(s) added")
                }
            }
        } else {
            toast("Checkup cancelled!")
        }

    }

    private fun updateCheckupForType(type: Int, data: Intent?) {
        when (type) {
            1 -> {
                // Nutrition
                val nutrition = data?.getParcelableExtra<Nutrition>(EXTRA_RESULT)
                checkup.nutrition = nutrition
            }
            2 -> {
                // Symptoms
                val symptoms = data?.getParcelableExtra<Symptoms>(EXTRA_RESULT)
                checkup.symptoms = symptoms
            }
            3 -> {
                // Supply
                val supply = data?.getParcelableExtra<Supply>(EXTRA_RESULT)
                checkup.supply = supply
            }
            5 -> {
                // Reminder
                val reminder = data?.getParcelableExtra<Reminder>(EXTRA_RESULT)
                checkup.reminder = reminder?.value
            }
        }
    }

    override fun onFailure(call: Call<Patient>?, t: Throwable?) {
        toast("Error creating checkup!")
    }

    override fun onResponse(call: Call<Patient>?, response: Response<Patient>?) {
        response?.let {
            toast("Checkup made successfully!")
            checkup = Checkup()
        }
    }

}