package com.jiovio.olivewear.rural.savemom.ui

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import com.jiovio.olivehealth.rural.savemom.R
import com.jiovio.olivewear.rural.savemom.api.model.Vitals
import kotlinx.android.synthetic.main.activity_vitals_summary.*

/**
 * Created by abara on 16/11/17.
 */
class VitalsSummaryActivity : AppCompatActivity() {

    companion object {
        const val EXTRA_PATIENT_NAME = "patient_name"
        const val EXTRA_AADHAR_NO = "aadhar_no"
        const val EXTRA_VITALS = "vitals"
        const val EXTRA_LAST_DATE = "last_date"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vitals_summary)
        vitals_summary_appbar.title = "Summary"
        setSupportActionBar(vitals_summary_appbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val patientName = intent.getStringExtra(EXTRA_PATIENT_NAME)
        vitals_summary_patientname.text = patientName

        val aadharNo = intent.getStringExtra(EXTRA_AADHAR_NO)
        vitals_summary_aadharno.text = aadharNo

        val lastDate = "Last update ${intent.getStringExtra(EXTRA_LAST_DATE)}"
        vital_summary_lastupdate_bp.text = lastDate
        vital_summary_lastupdate_weight.text = lastDate
        vital_summary_lastupdate_spo.text = lastDate

        val vitals = intent.getParcelableExtra<Vitals>(EXTRA_VITALS)

        val sys = vitals.bp?.systolic?.toInt() ?: 0
        val dia = vitals.bp?.diastolic?.toInt() ?: 0
        vitals_summary_value_bp.text = resources.getString(R.string.vitals_summary_bp, sys, dia)

        val weight = vitals.weight?.toInt() ?: 0
        vitals_summary_value_weight.text = resources.getString(R.string.vitals_summary_weight, weight)

        val spo = vitals.spo2?.value?.toInt() ?: 0
        vitals_summary_value_spo.text = resources.getString(R.string.vitals_summary_spo, spo)

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> finish()
        }
        return super.onOptionsItemSelected(item)
    }

}