package com.jiovio.olivewear.rural.savemom.fragment

import android.app.ProgressDialog
import android.os.Bundle
import android.view.View
import com.jiovio.olivehealth.rural.savemom.R
import com.jiovio.olivewear.rural.savemom.adapter.PatientListAdapter
import com.jiovio.olivewear.rural.savemom.api.ApiClient
import com.jiovio.olivewear.rural.savemom.api.ApiService
import com.jiovio.olivewear.rural.savemom.api.model.Patient
import com.jiovio.olivewear.rural.savemom.ui.NewUserActivity
import com.jiovio.olivewear.rural.savemom.util.accessTokenPref
import kotlinx.android.synthetic.main.fragment_list.*
import org.jetbrains.anko.bundleOf
import org.jetbrains.anko.support.v4.indeterminateProgressDialog
import org.jetbrains.anko.support.v4.intentFor
import org.jetbrains.anko.support.v4.toast
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by abara on 16/09/17.
 */
class PatientListFragment : ListFragment(), Callback<List<Patient>> {

    private lateinit var listPatientsCall: Call<List<Patient>>
    private var listAdapter: PatientListAdapter? = null
    private var dialog: ProgressDialog? = null

    companion object {
        const val TYPE_GROUP_CHECKUP = 1
        const val TYPE_FEEDBACK = 2
        private const val TYPE_FRAGMENT = "type_fragment"
        fun with(type: Int): PatientListFragment {
            val fragment = PatientListFragment()
            fragment.arguments = bundleOf(TYPE_FRAGMENT to type)
            return fragment
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        setTitle(R.string.today_group_checkup_patients_title)

        fragment_list_fab.visibility = View.VISIBLE

        if (listAdapter != null) {
            fragment_list.adapter = listAdapter
        } else {
            val service = ApiClient.client.create(ApiService::class.java)
            listPatientsCall = service.listPatients(activity!!.accessTokenPref)
            dialog = indeterminateProgressDialog("Loading patients...", "", {
                setCancelable(true)
                setOnCancelListener {
                    if (!listPatientsCall.isCanceled)
                        listPatientsCall.cancel()
                }
            })
            listPatientsCall.enqueue(this)
        }

        fragment_list_fab.setOnClickListener {
            startActivity(intentFor<NewUserActivity>())
        }

    }

    override fun onFailure(call: Call<List<Patient>>?, t: Throwable?) {
        toast("No patients")
        t?.printStackTrace()
        dialog?.dismiss()
    }

    override fun onResponse(call: Call<List<Patient>>?, response: Response<List<Patient>>?) {

        if (response != null) {

            val patients = response.body()

            if (patients != null) {
                listAdapter = PatientListAdapter(patients, { title, pos ->
                    fragment_list_fab.hide()
                    val patient = patients[pos]

                    val type = arguments!!.getInt(TYPE_FRAGMENT)

                    when (type) {
                        TYPE_GROUP_CHECKUP -> nextFragment(PatientsCheckupFragment.getInstance(title, patient))
                        TYPE_FEEDBACK -> nextFragment(CheckupListFragment.with(patient.name, patient.checkup, patient.aadharno))
                    }

                })
                fragment_list.adapter = listAdapter
            } else {
                toast("No patients available.")
            }

        } else {
            toast("Server error, try again.")
        }
        dialog?.dismiss()

    }

    override fun onDetach() {
        super.onDetach()
        if (!listPatientsCall.isCanceled)
            listPatientsCall.cancel()
    }

}