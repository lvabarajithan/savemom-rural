package com.jiovio.olivewear.rural.savemom

import android.app.Application
import com.jiovio.olivewear.rural.savemom.api.ApiClient
import retrofit2.Retrofit

/**
 * Created by abara on 16/08/17.
 */
class SaveMomApp : Application()