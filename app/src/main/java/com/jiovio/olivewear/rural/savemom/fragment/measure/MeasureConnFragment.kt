package com.jiovio.olivewear.rural.savemom.fragment.measure

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.jiovio.olivehealth.rural.savemom.R
import com.jiovio.olivewear.rural.savemom.fragment.BaseFragment
import com.jiovio.olivewear.rural.savemom.mcloud.MeasureHelper
import com.jiovio.olivewear.rural.savemom.ui.MeasureActivity.FragmentType.*
import com.jiovio.olivewear.rural.savemom.util.BluetoothUtils
import kotlinx.android.synthetic.main.activity_measure.*
import kotlinx.android.synthetic.main.fragment_measure_bp_connect.*
import org.jetbrains.anko.support.v4.toast

/**
 * Created by abara on 27/08/17.
 */
open class MeasureConnFragment : BaseFragment() {

    private var currentMeasureType = BP

    companion object {
        private val EXTRA_TYPE = "extra_type"
        fun getInstance(fragmentType: String): MeasureConnFragment {
            val fragment = MeasureConnFragment()
            val args = Bundle()
            args.putString(EXTRA_TYPE, fragmentType)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        currentMeasureType = valueOf(arguments!!.getString(EXTRA_TYPE, BP.name))
        return inflater.inflate(R.layout.fragment_measure_bp_connect, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        checkBluetooth()
        toolbarColor()

        setTitle(getTestTitle(currentMeasureType.name))

        val img = getImageType()
        Glide.with(this).load(img).into(measure_bp_connect_gif)

        val startButton = (activity)!!.measure_btn
        startButton.text = "Start"
        startButton.setOnClickListener {
            if (checkBluetooth()) {
                nextFragment(MeasureStartFragment.getInstance(currentMeasureType.name), false)
            } else {
                toast("Enable Bluetooth")
            }
        }
    }

    private fun getImageType() = when (currentMeasureType) {
        BP -> R.drawable.ic_connect_bp
        WEIGHT -> R.drawable.ic_connect_weight
        SPO -> R.drawable.ic_connect_spo
        else -> 0
    }

    fun getMeasureType(): Int = when (currentMeasureType) {
        SPO -> MeasureHelper.BLOOD_OXYGEN_ALL
        BG -> MeasureHelper.BLOOD_SUGAR
        TEMPERATURE -> MeasureHelper.BASE_TEMPERATURE
        else -> MeasureHelper.BLOOD_PRESSURE_ALL
    }

    protected fun checkBluetooth(): Boolean {
        if (!BluetoothUtils.checkPermission(activity!!)) {
            if (BluetoothUtils.enabled(this)) {
                BluetoothUtils.startDiscovery()
                return true
            }
        } else {
            if (BluetoothUtils.enabled(this)) {
                BluetoothUtils.startDiscovery()
                return true
            }
        }
        return false
    }

}