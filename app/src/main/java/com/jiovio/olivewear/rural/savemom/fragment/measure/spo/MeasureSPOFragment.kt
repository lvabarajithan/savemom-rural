package com.jiovio.olivewear.rural.savemom.fragment.measure.spo

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jiovio.olivehealth.rural.savemom.R
import com.jiovio.olivewear.rural.savemom.fragment.BaseFragment
import com.jiovio.olivewear.rural.savemom.fragment.measure.MeasureDoneFragment
import com.jiovio.olivewear.rural.savemom.ui.MeasureActivity
import kotlinx.android.synthetic.main.activity_measure.*

/**
 * Created by abara on 28/10/17.
 */
class MeasureSPOFragment : BaseFragment() {

    companion object {
        private val EXTRA_TYPE = "extra_type"
        fun getInstance(fragmentType: String): MeasureSPOFragment {
            val fragment = MeasureSPOFragment()
            val args = Bundle()
            args.putString(EXTRA_TYPE, fragmentType)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return null
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        setTitle(R.string.measure_title_spo2_test_appbar)

        val finishButton = (activity)!!.measure_btn
        finishButton.text = "Finish"
        finishButton.setOnClickListener {

            nextFragment(MeasureDoneFragment.getInstance(MeasureActivity.FragmentType.SPO.name, weightOrSpoPair = Pair(0, 0)), false)

        }

    }

}