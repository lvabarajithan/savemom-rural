package com.jiovio.olivewear.rural.savemom.fragment.measure

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jiovio.olivehealth.rural.savemom.R
import com.jiovio.olivewear.rural.savemom.fragment.BaseFragment
import com.jiovio.olivewear.rural.savemom.fragment.measure.bp.MeasureBPFragment
import com.jiovio.olivewear.rural.savemom.fragment.measure.spo.MeasureSPOFragment
import com.jiovio.olivewear.rural.savemom.fragment.measure.weight.MeasureHeightFragment
import com.jiovio.olivewear.rural.savemom.ui.MeasureActivity.FragmentType.*
import kotlinx.android.synthetic.main.activity_measure.*
import kotlinx.android.synthetic.main.fragment_measure_start.*
import org.jetbrains.anko.imageResource

/**
 * Created by abara on 28/10/17.
 */
class MeasureStartFragment : BaseFragment() {

    companion object {
        private val EXTRA_TYPE = "extra_type"
        fun getInstance(fragmentType: String): MeasureStartFragment {
            val fragment = MeasureStartFragment()
            val args = Bundle()
            args.putString(EXTRA_TYPE, fragmentType)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_measure_start, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val type = arguments!!.getString(EXTRA_TYPE)

        setTitle(getTestTitle(type))

        measure_start_image.imageResource = getImageType(type)
        measure_start_title.text = resources.getString(getTitleType(type))
        val startButton = (activity)!!.measure_btn
        startButton.text = "Start"
        startButton.setOnClickListener {
            nextFragment(getNextFragment(type), false)
        }
    }

    private fun getImageType(type: String) = when (type) {
        BP.name -> R.drawable.ic_start_bp
        SPO.name -> R.drawable.ic_start_spo
        WEIGHT.name -> R.drawable.ic_start_weight
        else -> 0
    }

    private fun getTitleType(type: String) = when (type) {
        BP.name -> R.string.measure_start_title_bp
        SPO.name -> R.string.measure_start_title_spo
        WEIGHT.name -> R.string.measure_start_title_weight
        else -> R.string.measure_unknown_test_title
    }

    private fun getNextFragment(type: String) = when (type) {
        BP.name -> MeasureBPFragment.getInstance(type)
        SPO.name -> MeasureSPOFragment()
        WEIGHT.name -> MeasureHeightFragment()
        else -> null
    }

}