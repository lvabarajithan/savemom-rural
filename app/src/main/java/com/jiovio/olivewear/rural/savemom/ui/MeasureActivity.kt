package com.jiovio.olivewear.rural.savemom.ui

import android.app.Activity
import android.content.Intent
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.MenuItem
import com.jiovio.olivehealth.rural.savemom.R
import com.jiovio.olivewear.rural.savemom.api.model.Vitals
import com.jiovio.olivewear.rural.savemom.fragment.BaseFragment
import com.jiovio.olivewear.rural.savemom.fragment.PatientsCheckupFragment
import com.jiovio.olivewear.rural.savemom.fragment.measure.MeasureIntroFragment
import com.jiovio.olivewear.rural.savemom.fragment.measure.bp.MeasureBPFragment
import com.jiovio.olivewear.rural.savemom.mcloud.MeasureHelper
import com.jiovio.olivewear.rural.savemom.util.commitTransaction
import com.jiovio.olivewear.rural.savemom.util.parseColor
import kotlinx.android.synthetic.main.activity_measure.*
import org.jetbrains.anko.textColor
import org.jetbrains.anko.toast

/**
 * Created by abara on 17/08/17.
 */
class MeasureActivity : BaseActivity(), MeasureBPFragment.OnMeasureSuccessListener {

    enum class FragmentType {
        HOME,
        IDENTITY,
        MEASURE_TYPE,
        BP,
        SPO,
        BG,
        TEMPERATURE,
        WEIGHT,
        SUMMARY,
        REPORT
    }

    private val vitals: Vitals = Vitals()
    private var lastWeight: Int? = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_measure)
        measure_appbar.title = ""
        setSupportActionBar(measure_appbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        lastWeight = intent.getIntExtra("last_weight", -1)
        if (lastWeight!! < 0) {
            lastWeight = null
        }

        loadFragment(MeasureIntroFragment.getInstance(FragmentType.BP.name), false)

    }

    /*
    * TODO: Implement measured values into patient.
    * */
    override fun onMeasureSuccess(fragmentType: Int, value: String) {
        // TODO: Insert measured values into Patient.
        // patient?.bp = value
        toast(value)
    }

    fun getVitals() = vitals

    fun getLastWeight() = lastWeight

    fun vitalsMeasureDone() {
        val data = Intent()
        data.putExtra(PatientsCheckupFragment.EXTRA_RESULT, vitals)
        setResult(Activity.RESULT_OK, data)
        finish()
        toast("Vitals checkup done.")
    }

    override fun changeToolbarColor(color: Int, titleColor: Int) {
        supportActionBar?.setBackgroundDrawable(ColorDrawable(parseColor(color)))
        measure_appbar_title.textColor = parseColor(titleColor)
    }

    override fun loadFragment(fragment: BaseFragment?, addToBackStack: Boolean) {
        fragment?.let {
            supportFragmentManager.commitTransaction {
                if (addToBackStack) addToBackStack(null)
                replace(R.id.measure_content, fragment)
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> finish()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onResume() {
        super.onResume()
        MeasureHelper.bind(this)
    }

    override fun onPause() {
        super.onPause()
        MeasureHelper.unbind(this)
    }

}