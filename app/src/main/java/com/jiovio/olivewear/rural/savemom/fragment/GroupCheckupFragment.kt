package com.jiovio.olivewear.rural.savemom.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jiovio.olivehealth.rural.savemom.R
import com.jiovio.olivewear.rural.savemom.ui.NewUserActivity
import kotlinx.android.synthetic.main.fragment_group_checkup.*
import org.jetbrains.anko.support.v4.intentFor

/**
 * Created by abara on 16/08/17.
 */
class GroupCheckupFragment : BaseFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val view = inflater.inflate(R.layout.fragment_group_checkup, container, false)
        setTitle(R.string.today_group_checkup_title)
        toolbarColor(R.color.colorIndigo, R.color.colorWhite)
        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        today_group_checkup_newuser.setOnClickListener {
            startActivity(intentFor<NewUserActivity>())
        }

        today_group_checkup_registereduser.setOnClickListener {
            nextFragment(AdhaarFragment())
        }

    }

}