package com.jiovio.olivewear.rural.savemom.ui

import android.support.v7.app.AppCompatActivity
import com.jiovio.olivehealth.rural.savemom.R
import com.jiovio.olivewear.rural.savemom.fragment.BaseFragment

/**
 * Created by abara on 17/08/17.
 */
abstract class BaseActivity : AppCompatActivity() {

    abstract fun changeToolbarColor(color: Int, titleColor: Int)

    abstract fun loadFragment(fragment: BaseFragment?, addToBackStack: Boolean = true)

    fun changeTheme(themeId: Int = R.style.AppTheme) {
        setTheme(themeId)
    }

}