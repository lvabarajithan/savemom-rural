package com.jiovio.olivewear.rural.savemom.adapter

import android.content.Context
import android.support.v7.widget.AppCompatImageView
import android.support.v7.widget.RecyclerView
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.bumptech.glide.Glide
import com.jiovio.olivehealth.rural.savemom.R
import com.jiovio.olivewear.rural.savemom.api.model.Report
import org.jetbrains.anko.find

/**
 * Created by abara on 05/11/17.
 */
class ReportsAdapter(private var reports: ArrayList<Report>?, private val addAction: () -> Unit, private val enableAdd: Boolean = true) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val TYPE_ADDER = 10
    private lateinit var context: Context

    init {
        if (reports == null) reports = ArrayList()
    }

    override fun getItemViewType(position: Int): Int = when (position) {
        reports!!.size -> TYPE_ADDER
        else -> 11
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        context = parent.context
        return when (viewType) {
            TYPE_ADDER -> {
                val view = LayoutInflater.from(context).inflate(R.layout.single_item_report_add, parent, false)
                AddReportsHolder(view)
            }
            else -> {
                val view = LayoutInflater.from(context).inflate(R.layout.single_item_report, parent, false)
                ReportsHolder(view)
            }
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder?, position: Int) {
        when (position) {
            reports!!.size -> {
                val addHolder = holder as AddReportsHolder
                addHolder.adderLayout.setOnClickListener {
                    addAction()
                }
            }
            else -> {
                val imageHolder = holder as ReportsHolder
                val byteArray = Base64.decode(reports!![position].reportimg, Base64.DEFAULT)
                Glide.with(context).asBitmap().load(byteArray).into(imageHolder.image)
            }
        }
    }

    override fun getItemCount(): Int = if (enableAdd) reports!!.size + 1 else reports!!.size

    class ReportsHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val image = itemView.find<AppCompatImageView>(R.id.item_report_image)
    }

    class AddReportsHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val adderLayout = itemView.find<LinearLayout>(R.id.item_report_add_layout)
    }

}